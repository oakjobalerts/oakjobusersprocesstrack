package com.projectmodel;

import java.util.ArrayList;
import java.util.HashMap;

public class ProviderToMainProviderModel {

	public ArrayList<String> main_providerList = new ArrayList<String>();
	public ArrayList<String> providerList = new ArrayList<String>();

	public HashMap<String, String> provider_to_main_provider_map = new HashMap<String, String>();

	public HashMap<String, String> main_provider_to_provider_map = new HashMap<String, String>();
	public HashMap<String, ArrayList<String>> main_provider_to_provider_list_map = new HashMap<String, ArrayList<String>>();

	public HashMap<String, ArrayList<String>> getMain_provider_to_provider_list_map() {
		return main_provider_to_provider_list_map;
	}

	public void setMain_provider_to_provider_list_map(
			HashMap<String, ArrayList<String>> main_provider_to_provider_list_map) {
		this.main_provider_to_provider_list_map = main_provider_to_provider_list_map;
	}

	public HashMap<String, String> getProvider_to_main_provider_map() {
		return provider_to_main_provider_map;
	}

	public void setProvider_to_main_provider_map(
			HashMap<String, String> provider_to_main_provider_map) {
		this.provider_to_main_provider_map = provider_to_main_provider_map;
	}

	public HashMap<String, String> getMain_provider_to_provider_map() {
		return main_provider_to_provider_map;
	}

	public void setMain_provider_to_provider_map(
			HashMap<String, String> main_provider_to_provider_map) {
		this.main_provider_to_provider_map = main_provider_to_provider_map;
	}

	public String provider;
	public String main_provider;
	public String new_Provider;
	public String new_mainProvider;

	public String getNew_Provider() {
		return new_Provider;
	}

	public void setNew_Provider(String new_Provider) {
		this.new_Provider = new_Provider;
	}

	public String getNew_mainProvider() {
		return new_mainProvider;
	}

	public void setNew_mainProvider(String new_mainProvider) {
		this.new_mainProvider = new_mainProvider;
	}

	public ArrayList<String> getMain_providerList() {
		return main_providerList;
	}

	public void setMain_providerList(ArrayList<String> main_providerList) {
		this.main_providerList = main_providerList;
	}

	public ArrayList<String> getProviderList() {
		return providerList;
	}

	public void setProviderList(ArrayList<String> providerList) {
		this.providerList = providerList;
	}

	public String getProvider() {
		return provider;
	}

	public void setProvider(String provider) {
		this.provider = provider;
	}

	public String getMain_provider() {
		return main_provider;
	}

	public void setMain_provider(String main_provider) {
		this.main_provider = main_provider;
	}

}
