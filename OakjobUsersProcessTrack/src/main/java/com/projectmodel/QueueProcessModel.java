package com.projectmodel;

public class QueueProcessModel {

	private String processName = "";
	private String processType = "";
	private String queueName = "";
	private String isActive = "";
	private String upTo = "";
	private String query = "";
	private String selecteddays = "";
	private String from = "";
	private String userType = "";
	private String providerFromQueue = "";
	private String j2cT2Value = "";
	private String jujuChannelValue = "";
	private String campaign_id = "";
	private String domain_name = "";
	private String sendgridMappingKey = "";
	private String pixelCategoryName = "";
	private String postalAddress = "";
	private String subjectFileName = "";
	private String white_label = "";
	private String dateCondition = "";
	private String liveRampPixelValue = "";
	private String advertisement = "";
	private String endDay = "";
	private String table_name = "";
	private String startDay = "";

	public String getStartDay() {
		return startDay;
	}

	public void setStartDay(String startDay) {
		this.startDay = startDay;
	}

	public String getTable_name() {
		return table_name;
	}

	public void setTable_name(String table_name) {
		this.table_name = table_name;
	}

	public String getEndDay() {
		return endDay;
	}

	public void setEndDay(String endDay) {
		this.endDay = endDay;
	}

	public String getLiveRampPixelValue() {
		return liveRampPixelValue;
	}

	public void setLiveRampPixelValue(String liveRampPixelValue) {
		this.liveRampPixelValue = liveRampPixelValue;
	}

	public String getWhite_label() {
		return white_label;
	}

	public void setWhite_label(String white_label) {
		this.white_label = white_label;
	}

	public String getDateCondition() {
		return dateCondition;
	}

	public void setDateCondition(String dateCondition) {
		this.dateCondition = dateCondition;
	}

	public String getProcessName() {
		return processName;
	}

	public void setProcessName(String processName) {
		this.processName = processName;
	}

	public String getProcessType() {
		return processType;
	}

	public void setProcessType(String processType) {
		this.processType = processType;
	}

	public String getQueueName() {
		return queueName;
	}

	public void setQueueName(String queueName) {
		this.queueName = queueName;
	}

	public String getIsActive() {
		return isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	public String getUpTo() {
		return upTo;
	}

	public void setUpTo(String upTo) {
		this.upTo = upTo;
	}

	public String getQuery() {
		return query;
	}

	public void setQuery(String query) {
		this.query = query;
	}

	public String getSelecteddays() {
		return selecteddays;
	}

	public void setSelecteddays(String selecteddays) {
		this.selecteddays = selecteddays;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public String getProviderFromQueue() {
		return providerFromQueue;
	}

	public void setProviderFromQueue(String providerFromQueue) {
		this.providerFromQueue = providerFromQueue;
	}

	public String getJ2cT2Value() {
		return j2cT2Value;
	}

	public void setJ2cT2Value(String j2cT2Value) {
		this.j2cT2Value = j2cT2Value;
	}

	public String getJujuChannelValue() {
		return jujuChannelValue;
	}

	public void setJujuChannelValue(String jujuChannelValue) {
		this.jujuChannelValue = jujuChannelValue;
	}

	public String getCampaign_id() {
		return campaign_id;
	}

	public void setCampaign_id(String campaign_id) {
		this.campaign_id = campaign_id;
	}

	public String getDomain_name() {
		return domain_name;
	}

	public void setDomain_name(String domain_name) {
		this.domain_name = domain_name;
	}

	public String getSendgridMappingKey() {
		return sendgridMappingKey;
	}

	public void setSendgridMappingKey(String sendgridMappingKey) {
		this.sendgridMappingKey = sendgridMappingKey;
	}

	public String getPixelCategoryName() {
		return pixelCategoryName;
	}

	public void setPixelCategoryName(String pixelCategoryName) {
		this.pixelCategoryName = pixelCategoryName;
	}

	public String getPostalAddress() {
		return postalAddress;
	}

	public void setPostalAddress(String postalAddress) {
		this.postalAddress = postalAddress;
	}

	public String getSubjectFileName() {
		return subjectFileName;
	}

	public void setSubjectFileName(String subjectFileName) {
		this.subjectFileName = subjectFileName;
	}

	public String getArborpixel() {
		return arborpixel;
	}

	public void setArborpixel(String arborpixel) {
		this.arborpixel = arborpixel;
	}

	public String getImage_postaladdress() {
		return image_postaladdress;
	}

	public void setImage_postaladdress(String image_postaladdress) {
		this.image_postaladdress = image_postaladdress;
	}

	public String getSendGridCategory() {
		return sendGridCategory;
	}

	public void setSendGridCategory(String sendGridCategory) {
		this.sendGridCategory = sendGridCategory;
	}

	private String arborpixel = "";
	private String image_postaladdress = "";
	private String sendGridCategory = "";
	private String open_pixelCategory = "";
	private String sendGrid_ip_pool = "";

	public String getSendGrid_ip_pool() {
		return sendGrid_ip_pool;
	}

	public void setSendGrid_ip_pool(String sendGrid_ip_pool) {
		this.sendGrid_ip_pool = sendGrid_ip_pool;
	}

	public String getOpen_pixelCategory() {
		return open_pixelCategory;
	}

	public void setOpen_pixelCategory(String open_pixelCategory) {
		this.open_pixelCategory = open_pixelCategory;
	}

	public String getAdvertisement() {
		return advertisement;
	}

	public void setAdvertisement(String advertisement) {
		this.advertisement = advertisement;
	}

}
