package com.projectmodel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class SearchResultModel {

	public HashMap<String, Integer> rampup_register_users = new HashMap<String, Integer>();
	public HashMap<String, Integer> rampup_freshers_users = new HashMap<String, Integer>();
	public HashMap<String, Integer> rampup_opener_users = new HashMap<String, Integer>();
	public HashMap<String, Integer> rampup_clicker_users = new HashMap<String, Integer>();
	public List<String> registerSerisData = new ArrayList<String>();
	public List<String> fresherSerisData = new ArrayList<String>();
	public List<String> openerSeriesData = new ArrayList<String>();
	public List<String> clickerSeriesData = new ArrayList<String>();
	public String whitelabelName;

	public String getWhitelabelName() {
		return whitelabelName;
	}

	public void setWhitelabelName(String whitelabelName) {
		this.whitelabelName = whitelabelName;
	}

	public List<String> getRegisterSerisData() {
		return registerSerisData;
	}

	public void setRegisterSerisData(List<String> registerSerisData) {
		this.registerSerisData = registerSerisData;
	}

	public List<String> getFresherSerisData() {
		return fresherSerisData;
	}

	public void setFresherSerisData(List<String> fresherSerisData) {
		this.fresherSerisData = fresherSerisData;
	}

	public List<String> getOpenerSeriesData() {
		return openerSeriesData;
	}

	public void setOpenerSeriesData(List<String> openerSeriesData) {
		this.openerSeriesData = openerSeriesData;
	}

	public List<String> getClickerSeriesData() {
		return clickerSeriesData;
	}

	public void setClickerSeriesData(List<String> clickerSeriesData) {
		this.clickerSeriesData = clickerSeriesData;
	}

	public ArrayList<String> dataInterValList = new ArrayList<String>();

	public ArrayList<String> getDataInterValList() {
		return dataInterValList;
	}

	public void setDataInterValList(ArrayList<String> dataInterValList) {
		this.dataInterValList = dataInterValList;
	}

	public HashMap<String, Integer> getRampup_register_users() {
		return rampup_register_users;
	}

	public void setRampup_register_users(
			HashMap<String, Integer> rampup_register_users) {
		this.rampup_register_users = rampup_register_users;
	}

	public HashMap<String, Integer> getRampup_freshers_users() {
		return rampup_freshers_users;
	}

	public void setRampup_freshers_users(
			HashMap<String, Integer> rampup_freshers_users) {
		this.rampup_freshers_users = rampup_freshers_users;
	}

	public HashMap<String, Integer> getRampup_opener_users() {
		return rampup_opener_users;
	}

	public void setRampup_opener_users(
			HashMap<String, Integer> rampup_opener_users) {
		this.rampup_opener_users = rampup_opener_users;
	}

	public HashMap<String, Integer> getRampup_clicker_users() {
		return rampup_clicker_users;
	}

	public void setRampup_clicker_users(
			HashMap<String, Integer> rampup_clicker_users) {
		this.rampup_clicker_users = rampup_clicker_users;
	}

}
