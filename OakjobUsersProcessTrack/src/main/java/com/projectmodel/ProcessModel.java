package com.projectmodel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ProcessModel {
	HashMap<String, String> whitelable_table_map;
	HashMap<String, ArrayList<String>> whitelable_domainHashMap;
	HashMap<String, String> whitelable_domain_with_type;

	public HashMap<String, String> getWhitelable_domain_with_type() {
		return whitelable_domain_with_type;
	}

	public void setWhitelable_domain_with_type(
			HashMap<String, String> whitelable_domain_with_type) {
		this.whitelable_domain_with_type = whitelable_domain_with_type;
	}

	public HashMap<String, String> getWhitelable_table_map() {
		return whitelable_table_map;
	}

	public HashMap<String, ArrayList<String>> getWhitelable_domainHashMap() {
		return whitelable_domainHashMap;
	}

	public void setWhitelable_domainHashMap(
			HashMap<String, ArrayList<String>> whitelable_domainHashMap) {
		this.whitelable_domainHashMap = whitelable_domainHashMap;
	}

	public void setWhitelable_table_map(
			HashMap<String, String> whitelable_table_map) {
		this.whitelable_table_map = whitelable_table_map;
	}

	public ArrayList<String> getWhitelabelList() {
		return whitelabelList;
	}

	public void setWhitelabelList(ArrayList<String> whitelabelList) {
		this.whitelabelList = whitelabelList;
	}

	public ArrayList<String> getProcessType() {
		return processTypes;
	}

	public void setProcessType(ArrayList<String> processType) {
		this.processTypes = processType;
	}

	public ArrayList<String> getAdvertiseMents() {
		return advertiseMents;
	}

	public void setAdvertiseMents(ArrayList<String> advertiseMents) {
		this.advertiseMents = advertiseMents;
	}

	public ArrayList<String> getQueueNames() {
		return queueNames;
	}

	public void setQueueNames(ArrayList<String> queueNames) {
		this.queueNames = queueNames;
	}

	ArrayList<String> whitelabelList;
	ArrayList<String> processTypes;
	ArrayList<String> advertiseMents;
	ArrayList<String> queueNames;

	public String whitelable_name = "";
	public String advertisement = "";
	public String process_Type = "";
	public String fresher_domain = "";
	public String opener_domain = "";
	public String fresher_second_doman = "";
	public String queue_name = "";
	public String weekly_domain = "";
	public String table_name = "";
	public String entered_provider = "";

	public String getEntered_provider() {
		return entered_provider;
	}

	public void setEntered_provider(String entered_provider) {
		this.entered_provider = entered_provider;
	}

	public String getTable_name() {
		return table_name;
	}

	public void setTable_name(String table_name) {
		this.table_name = table_name;
	}

	public String getWeekly_domain() {
		return weekly_domain;
	}

	public void setWeekly_domain(String weekly_domain) {
		this.weekly_domain = weekly_domain;
	}

	public ArrayList<String> getProcessTypes() {
		return processTypes;
	}

	public void setProcessTypes(ArrayList<String> processTypes) {
		this.processTypes = processTypes;
	}

	public String getWhitelable_name() {
		return whitelable_name;
	}

	public void setWhitelable_name(String whitelable_name) {
		this.whitelable_name = whitelable_name;
	}

	public String getAdvertisement() {
		return advertisement;
	}

	public void setAdvertisement(String advertisement) {
		this.advertisement = advertisement;
	}

	public String getProcess_Type() {
		return process_Type;
	}

	public void setProcess_Type(String process_Type) {
		this.process_Type = process_Type;
	}

	public String getFresher_domain() {
		return fresher_domain;
	}

	public void setFresher_domain(String fresher_domain) {
		this.fresher_domain = fresher_domain;
	}

	public String getOpener_domain() {
		return opener_domain;
	}

	public void setOpener_domain(String opener_domain) {
		this.opener_domain = opener_domain;
	}

	public String getFresher_second_doman() {
		return fresher_second_doman;
	}

	public void setFresher_second_doman(String fresher_second_doman) {
		this.fresher_second_doman = fresher_second_doman;
	}

	public String getQueue_name() {
		return queue_name;
	}

	public void setQueue_name(String queue_name) {
		this.queue_name = queue_name;
	}

}
