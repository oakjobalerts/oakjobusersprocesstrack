package com.projectmodel;

import java.util.HashSet;
import java.util.Set;

public class SeekersModel {

	public String seekerName = "";
	public String channel = "";
	public String postal_address = "";
	public String image_postal_address = "";
	public String subjectAndFromName = "";
	public String arbor_pixel = "";

	public Set<String> seekerList = new HashSet<String>();
	public Set<String> channelList = new HashSet<String>();
	public Set<String> postal_addressList = new HashSet<String>();
	public Set<String> image_postal_addressList = new HashSet<String>();
	public Set<String> subjectAndFromNameList = new HashSet<String>();
	public Set<String> arbor_pixelList = new HashSet<String>();
	
	

	public Set<String> getSeekerList() {
		return seekerList;
	}

	public void setSeekerList(Set<String> seekerList) {
		this.seekerList = seekerList;
	}

	public Set<String> getChannelList() {
		return channelList;
	}

	public void setChannelList(Set<String> channelList) {
		this.channelList = channelList;
	}

	public Set<String> getPostal_addressList() {
		return postal_addressList;
	}

	public void setPostal_addressList(Set<String> postal_addressList) {
		this.postal_addressList = postal_addressList;
	}

	public Set<String> getImage_postal_addressList() {
		return image_postal_addressList;
	}

	public void setImage_postal_addressList(Set<String> image_postal_addressList) {
		this.image_postal_addressList = image_postal_addressList;
	}

	public Set<String> getSubjectAndFromNameList() {
		return subjectAndFromNameList;
	}

	public void setSubjectAndFromNameList(Set<String> subjectAndFromNameList) {
		this.subjectAndFromNameList = subjectAndFromNameList;
	}

	public Set<String> getArbor_pixelList() {
		return arbor_pixelList;
	}

	public void setArbor_pixelList(Set<String> arbor_pixelList) {
		this.arbor_pixelList = arbor_pixelList;
	}

	public String getSeekerName() {
		return seekerName;
	}

	public void setSeekerName(String seekerName) {
		this.seekerName = seekerName;
	}

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public String getPostal_address() {
		return postal_address;
	}

	public void setPostal_address(String postal_address) {
		this.postal_address = postal_address;
	}

	public String getImage_postal_address() {
		return image_postal_address;
	}

	public void setImage_postal_address(String image_postal_address) {
		this.image_postal_address = image_postal_address;
	}

	public String getSubjectAndFromName() {
		return subjectAndFromName;
	}

	public void setSubjectAndFromName(String subjectAndFromName) {
		this.subjectAndFromName = subjectAndFromName;
	}

	public String getArbor_pixel() {
		return arbor_pixel;
	}

	public void setArbor_pixel(String arbor_pixel) {
		this.arbor_pixel = arbor_pixel;
	}
}
