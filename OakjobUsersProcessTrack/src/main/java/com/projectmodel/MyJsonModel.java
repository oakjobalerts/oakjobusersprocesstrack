package com.projectmodel;

import java.util.HashMap;
import java.util.Map;

public class MyJsonModel {
	public Map<String, String> advertisement_whitelabel_map = new HashMap<String, String>();

	public Map<String, String> getAdvertisement_whitelabel_map() {
		return advertisement_whitelabel_map;
	}

	public void setAdvertisement_whitelabel_map(
			Map<String, String> advertisement_whitelabel_map) {
		this.advertisement_whitelabel_map = advertisement_whitelabel_map;
	}

}
