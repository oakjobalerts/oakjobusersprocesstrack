package com.projectmodel;

import java.util.ArrayList;
import java.util.HashMap;

public class AdvertisementModel {
	private ArrayList<String> advertisements;
	private ArrayList<String> whitelables;
	private String selectedWhitelable;
	private String selectedAdvertisement;
	private String fromDateSelected;
	private String toDateSelected;
	public HashMap<String, ArrayList<String>> advertisement_wise_whitelables_list = new HashMap<String, ArrayList<String>>();
	private MyJsonModel myJsonModel = new MyJsonModel();

	public MyJsonModel getMyJsonModel() {
		return myJsonModel;
	}

	public HashMap<String, ArrayList<String>> getAdvertisement_wise_whitelables_list() {
		return advertisement_wise_whitelables_list;
	}

	public void setAdvertisement_wise_whitelables_list(
			HashMap<String, ArrayList<String>> advertisement_wise_whitelables_list) {
		this.advertisement_wise_whitelables_list = advertisement_wise_whitelables_list;
	}

	public void setMyJsonModel(MyJsonModel myJsonModel) {
		this.myJsonModel = myJsonModel;
	}

	public String getFromDateSelected() {
		return fromDateSelected;
	}

	public void setFromDateSelected(String fromDateSelected) {
		this.fromDateSelected = fromDateSelected;
	}

	public String getToDateSelected() {
		return toDateSelected;
	}

	public void setToDateSelected(String toDateSelected) {
		this.toDateSelected = toDateSelected;
	}

	HashMap<String, String> whitelable_table_map;

	public ArrayList<String> getAdvertisements() {
		return advertisements;
	}

	public void setAdvertisements(ArrayList<String> advertisements) {
		this.advertisements = advertisements;
	}

	public ArrayList<String> getWhitelables() {
		return whitelables;
	}

	public void setWhitelables(ArrayList<String> whitelables) {
		this.whitelables = whitelables;
	}

	public String getSelectedWhitelable() {
		return selectedWhitelable;
	}

	public void setSelectedWhitelable(String selectedWhitelable) {
		this.selectedWhitelable = selectedWhitelable;
	}

	public String getSelectedAdvertisement() {
		return selectedAdvertisement;
	}

	public void setSelectedAdvertisement(String selectedAdvertisement) {
		this.selectedAdvertisement = selectedAdvertisement;
	}

	public HashMap<String, String> getWhitelable_table_map() {
		return whitelable_table_map;
	}

	public void setWhitelable_table_map(
			HashMap<String, String> whitelable_table_map) {
		this.whitelable_table_map = whitelable_table_map;
	}

}
