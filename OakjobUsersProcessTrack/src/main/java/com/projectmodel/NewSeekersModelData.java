package com.projectmodel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class NewSeekersModelData {

	private HashMap<String, SeekersModel> seekers_map = new HashMap<String, SeekersModel>();

	private List<SeekersModel> seekersList = new ArrayList<SeekersModel>();

	
	public List<SeekersModel> getSeekersList() {
		return seekersList;
	}

	public void setSeekersList(List<SeekersModel> seekersList) {
		this.seekersList = seekersList;
	}

	public HashMap<String, SeekersModel> getSeekers_map() {
		return seekers_map;
	}

	public void setSeekers_map(HashMap<String, SeekersModel> seekers_map) {
		this.seekers_map = seekers_map;
	}

}
