package com.projectmodel;

public class ProviderBaseModel {

	private String providerName;
	private String displayProviderName;
	private int position;
	private boolean update;

	public boolean isUpdate() {
		return update;
	}

	public void setUpdate(boolean isUpdate) {
		this.update = isUpdate;
	}

	public int getPosition() {
		return position;
	}

	public void setPosition(int position) {
		this.position = position;
	}

	public String getProviderName() {
		return providerName;
	}

	public void setProviderName(String providerName) {
		this.providerName = providerName;
	}

	public String getDisplayProviderName() {
		return displayProviderName;
	}

	public void setDisplayProviderName(String displayProviderName) {
		this.displayProviderName = displayProviderName;
	}

}
