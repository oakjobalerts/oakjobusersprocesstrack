package com.project.utility;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import com.google.gson.Gson;
import com.projectmodel.AdvertisementModel;
import com.projectmodel.MyJsonModel;
import com.projectmodel.ProviderBaseModel;
import com.projectmodel.QueueProcessModel;

public class Utility {

	public static HashMap<String, ArrayList<Integer>> domain_CompaignWise_Map = new HashMap<String, ArrayList<Integer>>();
	public static HashMap<String, String> advertisement_wise_info = new HashMap<String, String>();
	public static HashMap<String, String> domain_and_emailclient = new HashMap<String, String>();
	public static ArrayList<Integer> sendgridMappingKey = new ArrayList<Integer>();
	public static String user_process_life_cycle_dummy = "user_process_lifecycle_dummy";
	public static String white_label_queue_process = "white_label_queue_process";
	public static String web_alerts_table = "tbl_jobAlerts_whitelabels";

	public static Set<String> seekerList = new HashSet<String>();
	public static Set<String> channelList = new HashSet<String>();
	public static Set<String> postal_addressList = new HashSet<String>();
	public static Set<String> image_postal_addressList = new HashSet<String>();
	public static Set<String> subjectAndFromNameList = new HashSet<String>();
	public static Set<String> arbor_pixelList = new HashSet<String>();

	public static String userName = "fbg-user";
	public static String passWord = "sarasota97$";

	public static String internal_userName = "test";
	public static String internal_passWord = "test";

	public static String filePath = "/var/nfs-93/redirect/mis_logs/EditAlertProject/myfile.json";
	 public static String provider_to_mainProvider_file =
	 "/var/nfs-93/redirect/mis_logs/chartgraph_java/main_provider/mainprovider_chartGraph.txt";

	// =======================localsytem=========================
//	 public static String filePath =
//	 "/home/signity/Desktop/pawan_data/myfile.json";

	// public static String provider_to_mainProvider_file =
	// "/home/signity/Desktop/mainprovider_chartGraph.txt";

	// =======================localsytem=========================
	static String user = "stats@fbgreports.com";
	static String password = "sarasotA97";
	static String host = "mail.fbgreports.com";
	static String protocol = "smtp";
	static String port = "465";
	static boolean auth = true;
	static String socketFactory_port = "465";
	static String socet_factory_class = "javax.net.ssl.SSLSocketFactory";
	public static ArrayList<String> cc_mail_list = new ArrayList<String>();
	public static String to_email_receiver = "pawan@signitysolutions.co.in";
	public static String updated_html = "";
	public static List<ProviderBaseModel> last_provider_list = new ArrayList<ProviderBaseModel>();

	public static Connection openConnection() {
		String HOST_NAME = "oakuserdbinstance-cluster.cluster-ca2bixk3jmwi.us-east-1.rds.amazonaws.com:3306";
		String DB_NAME = "oakalerts";
		String username = "awsoakuser";
		String password = "awsoakusersignity";
		String url1 = "jdbc:mysql://" + HOST_NAME + "/" + DB_NAME;
		Connection con = null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			con = (Connection) DriverManager.getConnection(url1, username,
					password);
		} catch (Exception e) {
			e.printStackTrace();

		}
		return con;
	}

	public static void closeConnection(Connection con) {
		try {
			con.close();
		} catch (Exception e) {
		}
	}

	public static QueueProcessModel setCampaignId(QueueProcessModel model) {

		ArrayList<Integer> list = Utility.domain_CompaignWise_Map.get(model
				.getDomain_name());

		System.out.println("size of compaing list in domain="
				+ model.getDomain_name() + " " + list.size());

		Collections.sort(list);
		Collections.reverse(list);

		System.out.println("last campaing id=" + list.get(0));

		model.setCampaign_id(String.valueOf(list.get(0) + 1));
		list.add(Integer.parseInt(model.getCampaign_id()));
		Utility.domain_CompaignWise_Map.put(model.getDomain_name(), list);

		System.out.println("new compaign=" + model.getCampaign_id());

		return model;

	}

	public static QueueProcessModel setSendGridMappingKey(
			QueueProcessModel model) {

		System.out.println("size of sendgrid key="
				+ Utility.sendgridMappingKey.size());
		Collections.sort(Utility.sendgridMappingKey);
		Collections.reverse(Utility.sendgridMappingKey);

		System.out.println("last mapping id="
				+ Utility.sendgridMappingKey.get(0));

		model.setSendgridMappingKey(String.valueOf(Utility.sendgridMappingKey
				.get(0) + 1));

		Utility.sendgridMappingKey.add(Integer.parseInt(model
				.getSendgridMappingKey()));

		Collections.sort(Utility.sendgridMappingKey);
		Collections.reverse(Utility.sendgridMappingKey);

		System.out.println("new mapping id="
				+ Utility.sendgridMappingKey.get(0));

		System.out.println("size of sendgrid key="
				+ Utility.sendgridMappingKey.size());

		return model;

	}

	public static QueueProcessModel setSendgridCategory(
			QueueProcessModel model, String emailClient) {
		System.out.println("email client from sendgrdid category="
				+ emailClient);
		if (emailClient.toLowerCase().equalsIgnoreCase("sendgrid")) {
			model.setSendGridCategory(model.getWhite_label() + "-"
					+ model.getAdvertisement() + "-" + model.getProcessType());
		} else if (emailClient.toLowerCase().equalsIgnoreCase("sparkpost")) {
			System.out.println("email client from sendgrdid category="
					+ emailClient);
			model.setSendGridCategory("sparkpost");

		}

		return model;

	}

	public static ArrayList<String> createDataInterval(String from, String to) {
		Date todaydate = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Calendar cal = Calendar.getInstance();
		ArrayList<String> list = new ArrayList<String>();
		Date fromDate = null;
		try {
			if (from.equals("") && to.equals("")) {
				cal.set(Calendar.DAY_OF_MONTH,
						cal.get(Calendar.DAY_OF_MONTH) - 6);
				fromDate = cal.getTime();
			} else if (!from.equals("") && to.equals(""))
				fromDate = sdf.parse(from);
			else {
				fromDate = sdf.parse(from);
				todaydate = sdf.parse(to);
			}
			cal.setTime(fromDate);
			while (cal.getTime().before(todaydate)) {
				String str = sdf.format(cal.getTime());
				list.add(str);
				System.out.println(str);
				cal.add(Calendar.DATE, 1);
			}
			if (((from == null) && (to == null))
					|| ((from != null) && (to != null))) {
				String str = sdf.format(cal.getTime());
				list.add(str);
				System.out.println(str);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

	public static MyJsonModel create_My_Jason_Model(String path) {

		InputStream in;
		StringBuilder out = new StringBuilder();
		try {
			in = new FileInputStream(new File(path));
			BufferedReader reader = new BufferedReader(
					new InputStreamReader(in));

			String line;
			while ((line = reader.readLine()) != null) {
				out.append(line);
			}
			System.out.println(out.toString());

			reader.close();
			// Prints the string content
			// read
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Gson gson = new Gson();
		MyJsonModel[] data = gson.fromJson(out.toString(), MyJsonModel[].class);
		// from input stream

		return data[0];
	}

	public static AdvertisementModel create_map_advertisement_with_whitelable(
			AdvertisementModel model) {

		for (Map.Entry<String, String> entry : model.getMyJsonModel()
				.getAdvertisement_whitelabel_map().entrySet()) {

			try {
				Integer intValue = Integer.parseInt(entry.getKey());

			} catch (Exception e) {
				// TODO: handle exception

				String string_key = entry.getKey().toLowerCase();
				String string_value = entry.getValue().toLowerCase();

				String dataArray[] = string_value.split("\\|");

				ArrayList<String> lastList_of_whitelabels = model
						.getAdvertisement_wise_whitelables_list().get(
								string_key);
				if (lastList_of_whitelabels == null) {

					lastList_of_whitelabels = new ArrayList<String>();

				}

				lastList_of_whitelabels.addAll(Arrays.asList(dataArray));

				model.getAdvertisement_wise_whitelables_list().put(string_key,
						lastList_of_whitelabels);
			}

		}
		return model;
	}

	public static String sendSmtpEmail(String subject, String body) {
		System.out.println("sending mail");
		Properties props = System.getProperties();

		props.put("mail.smtp.host", host);
		props.put("mail.transport.protocol", protocol);
		props.put("mail.smtp.port", port);
		props.put("mail.smtp.auth", auth);
		props.put("mail.smtp.password", password);
		props.put("mail.smtp.socketFactory.port", socketFactory_port);
		props.put("mail.smtp.socketFactory.class", socet_factory_class);

		Authenticator auth = new Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(user, password);
			}
		};

		Session session = Session.getInstance(props, auth);

		try {
			MimeMessage msg = new MimeMessage(session);

			for (String cc : Utility.cc_mail_list) {
				msg.addRecipients(Message.RecipientType.CC,
						InternetAddress.parse(cc));
			}
			//
			// for (String bcc : emailReceiver.getBccList()) {
			// msg.addRecipients(Message.RecipientType.BCC,
			// InternetAddress.parse(bcc));
			// }

			msg.addHeader("Content-type", "text/HTML; charset=UTF-8");
			msg.addHeader("format", "flowed");
			msg.addHeader("Content-Transfer-Encoding", "8bit");
			msg.setSubject(subject, "UTF-8");
			msg.setContent(body, "text/html");

			System.out.println(body);

			msg.setFrom(new InternetAddress(user, "FBG Reports"));
			msg.setReplyTo(InternetAddress.parse(user, false));

			msg.setSentDate(new Date());

			// if (list.size() != 0) {
			//
			// MimeBodyPart messageBodyPart = new MimeBodyPart();
			// DataSource ds = new ByteArrayDataSource(new
			// GsonBuilder().setPrettyPrinting().create().toJson(list),
			// "application/json");
			// messageBodyPart.setDataHandler(new DataHandler(ds));
			// messageBodyPart.setFileName("jobsExportSources.json");
			//
			// MimeBodyPart textBodyPart = new MimeBodyPart();
			// textBodyPart.setContent(body, "text/html");
			//
			// MimeMultipart mimeMultipart = new MimeMultipart();
			// mimeMultipart.addBodyPart(messageBodyPart);
			// mimeMultipart.addBodyPart(textBodyPart);
			//
			// msg.setContent(mimeMultipart);
			// }

			msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to_email_receiver, false));
			Transport.send(msg);

			System.out.println("Email Sent Successfully!!");

		} catch (Exception e) {
			e.printStackTrace();
		}

		return "Email Sent Successfully!!";
	}
}
