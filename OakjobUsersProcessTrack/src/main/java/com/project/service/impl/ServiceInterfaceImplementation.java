package com.project.service.impl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.project.service.ServicesInterface;
import com.project.utility.Utility;
import com.projectmodel.AdvertisementModel;
import com.projectmodel.NewSeekersModelData;
import com.projectmodel.ProcessModel;
import com.projectmodel.ProviderBaseModel;
import com.projectmodel.ProviderToMainProviderModel;
import com.projectmodel.QueueProcessModel;
import com.projectmodel.SearchResultModel;
import com.projectmodel.SeekersModel;

public class ServiceInterfaceImplementation implements ServicesInterface {

	// this is use to initialize the whitlable with table map
	@Override
	public HashMap<String, String> getWhitelable_table_map() {
		// TODO Auto-generated method stub

		String query = "";
		HashMap<String, String> whitelable_table_map = new HashMap<String, String>();

		query = "select whitelabel_name,table_name from whitelabels_process where whitelabel_name is not null group by 1";

		System.out.println("rampup-query= " + query);

		Connection con = Utility.openConnection();

		try {
			if (con != null && !con.isClosed()) {

				java.sql.Statement st = con.createStatement();

				ResultSet res = st.executeQuery(query);

				while (!res.isClosed() && res.next()) {

					String whitelable = res.getString("whitelabel_name");
					String tableArray[] = res.getString("table_name").split(
							"\\|");
					String table = "";

					if (tableArray.length > 1) {
						table = tableArray[1].trim();
					} else {
						table = tableArray[0].trim();
					}

					whitelable_table_map.put(whitelable, table);

				}

			}
			if (con != null) {
				con.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return whitelable_table_map;
	}

	// this is use to initialize the rampup with table map
	@Override
	public ArrayList<String> get_new_rampup_advertisement() {
		// TODO Auto-generated method stub

		String query = "";
		ArrayList<String> advertisement = new ArrayList<String>();

		query = "select advertisement from tbl_job_seekers_common_import where advertisement!='' and (advertisement=advertisement_for_code or advertisement_for_code='') group by 1";

		System.out.println("rampup-query= " + query);

		Connection con = Utility.openConnection();

		try {
			if (con != null && !con.isClosed()) {

				java.sql.Statement st = con.createStatement();

				ResultSet res = st.executeQuery(query);

				while (!res.isClosed() && res.next()) {

					advertisement.add(res.getString("advertisement"));

				}

			}
			if (con != null) {
				con.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		System.out.println("advertisement.size()" + advertisement.size());

		return advertisement;
	}

	@Override
	public ArrayList<String> getWhitelabesList(HashMap<String, String> map) {
		// TODO Auto-generated method stub
		ArrayList<String> whitelableList = new ArrayList<String>();
		Map<String, String> localmap = map;
		for (Map.Entry<String, String> entry : localmap.entrySet()) {

			whitelableList.add(entry.getKey());

		}
		return whitelableList;
	}

	@Override
	public ArrayList<String> getQueueNamesList() {
		// TODO Auto-generated method stub

		String query = "";
		ArrayList<String> queueName = new ArrayList<String>();

		query = "select QueueName from "
				+ Utility.user_process_life_cycle_dummy + " group by 1";

		System.out.println("rampup-query= " + query);

		Connection con = Utility.openConnection();

		try {
			if (con != null && !con.isClosed()) {

				java.sql.Statement st = con.createStatement();

				ResultSet res = st.executeQuery(query);

				while (!res.isClosed() && res.next()) {

					queueName.add(res.getString("QueueName"));

				}

			}
			if (con != null) {
				con.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return queueName;
	}

	@Override
	public HashMap<String, ArrayList<String>> getWhitelabelWith_domain() {
		// TODO Auto-generated method stub
		HashMap<String, ArrayList<String>> localHashMap = new HashMap<String, ArrayList<String>>();
		String query = "select whitelabel_name,DomainUrl from whitelabels_process group by 1,2";

		System.out.println(query);

		Connection con = Utility.openConnection();

		try {
			if (con != null && !con.isClosed()) {

				java.sql.Statement st = con.createStatement();

				ResultSet res = st.executeQuery(query);
				ArrayList<String> localDomainList = null;

				while (!res.isClosed() && res.next()) {

					try {
						System.out.println(res.getString("whitelabel_name"));

						localDomainList = localHashMap.get(res.getString(
								"whitelabel_name").trim());
					} catch (Exception e) {
						// TODO Auto-generated catch block
					}

					if (localDomainList == null) {
						localDomainList = new ArrayList<String>();

					}
					if (res.getString("DomainUrl") != null) {
						localDomainList.add(res.getString("DomainUrl").trim());
						localHashMap.put(res.getString("whitelabel_name")
								.trim(), localDomainList);
					}

				}

			}
			if (con != null) {
				con.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return localHashMap;
	}

	@Override
	public String addNewProcessInTable(ProcessModel model) {
		// TODO Auto-generated method stub

		// in this we are creating first list of new model object with all types
		// then we iterate loop and insert the respective record
		String resultString = "";
		ArrayList<QueueProcessModel> localList = new ArrayList<QueueProcessModel>();

		System.out.println(model.getWhitelable_name() + "|"
				+ model.getProcess_Type() + "|" + model.getAdvertisement()
				+ "|" + model.getQueue_name() + "|");

		if (model.getFresher_domain().contains("-")) {

			model.setFresher_domain(model.getFresher_domain().split("-")[0]);

		}
		if (model.getFresher_second_doman().contains("-")) {

			model.setFresher_second_doman(model.getFresher_second_doman()
					.split("-")[0]);

		}
		if (model.getOpener_domain().contains("-")) {

			model.setOpener_domain(model.getOpener_domain().split("-")[0]);

		}
		if (model.getWeekly_domain().contains("-")) {

			model.setWeekly_domain(model.getWeekly_domain().split("-")[0]);

		}

		String whereClause = " advertisement =\\'"
				+ model.getAdvertisement() + "\\'";
		String typeClause = model.getAdvertisement();
		String seekerWiseDataKey = model.getAdvertisement();

		if (!model.getEntered_provider().equalsIgnoreCase("")) {
			whereClause = " provider =\\'"
					+ model.getEntered_provider() + "\\'";
			typeClause = model.getEntered_provider();
			seekerWiseDataKey = model.getWhitelable_name();
		}

		System.out.println("where clause=" + whereClause);

		for (int i = 0; i < 4; i++) {
			QueueProcessModel newQueueModel = new QueueProcessModel();
			newQueueModel.setProcessName(model.getWhitelable_name());
			newQueueModel.setWhite_label(model.getWhitelable_name());
			newQueueModel.setAdvertisement(model.getAdvertisement());
			newQueueModel.setTable_name(model.getTable_name());
			newQueueModel.setUserType(model.getWhitelable_name());
			newQueueModel.setOpen_pixelCategory(model.getWhitelable_name());

			if (i == 0) {
				newQueueModel.setProcessType("daily-"
						+ typeClause);
				newQueueModel
						.setQuery("SELECT DISTINCT count(*) AS rowcount FROM "
								+ model.getTable_name()
								+ " t2 WHERE (LENGTH(KEYWORD)>2) AND (frequency=1) and (open=0 and click=0)"
								+ " and" + whereClause);
				newQueueModel
						.setDateCondition("t2.date >= now()-interval # day");
				newQueueModel.setDomain_name(model.getFresher_domain());
				newQueueModel.setEndDay("3");
				newQueueModel.setStartDay("0");

			}

			if (i == 1) {
				newQueueModel.setProcessType("opener-"
						+ typeClause);
				newQueueModel
						.setQuery("SELECT DISTINCT count(*) AS rowcount FROM "
								+ model.getTable_name()
								+ " t2 WHERE (LENGTH(KEYWORD)>2) AND (frequency=1) and (open=1 and click=0)"
								+ " and" + whereClause);
				newQueueModel
						.setDateCondition("t2.open_at > now()-interval # day");

				newQueueModel.setDomain_name(model.getOpener_domain());
				newQueueModel.setEndDay("30");
				newQueueModel.setStartDay("0");

			}
			if (i == 2) {
				newQueueModel.setProcessType("clicker-"
						+ typeClause);
				newQueueModel
						.setQuery("SELECT DISTINCT count(*) AS rowcount FROM "
								+ model.getTable_name()
								+ " t2 WHERE (LENGTH(KEYWORD)>2) AND (frequency=1) and (open=1 and click=1)"
								+ " and" + whereClause);

				newQueueModel
						.setDateCondition("t2.open_at > now()-interval # day");
				newQueueModel.setDomain_name(model.getOpener_domain());
				newQueueModel.setEndDay("30");
				newQueueModel.setStartDay("0");
			}
			if (i == 3) {
				newQueueModel.setProcessType("daily-second-"
						+ typeClause);
				newQueueModel
						.setQuery("SELECT DISTINCT count(*) AS rowcount FROM "
								+ model.getTable_name()
								+ " t2 WHERE (LENGTH(KEYWORD)>2) AND (frequency=1) and (open=0 and click=0)"
								+ " and" + whereClause);
				newQueueModel
						.setDateCondition("t2.date < now()-interval # day and t2.date >= now()-interval # day");
				newQueueModel.setDomain_name(model.getFresher_second_doman());
				newQueueModel.setEndDay("11");
				newQueueModel.setStartDay("3");

			}

			System.out.println("getting advertisement info for adver="
					+ seekerWiseDataKey);

			String dataArray[] = null;

			String data = Utility.advertisement_wise_info.get(seekerWiseDataKey.toLowerCase());
			if (data != null) {
				try {
					dataArray = data.split("\\|");
					newQueueModel.setJujuChannelValue(dataArray[0]);
					newQueueModel.setJ2cT2Value("&t2=" + dataArray[0]);
					newQueueModel.setPostalAddress(dataArray[1]);
					newQueueModel.setImage_postaladdress(dataArray[2]);
					newQueueModel.setSubjectFileName(dataArray[3]);
					newQueueModel.setArborpixel(dataArray[4]);
				} catch (Exception e) {
					// TODO Auto-generated catch block
				}
			} else {
				localList.clear();
				return "no advertisement data found for "
						+ model.getAdvertisement().toLowerCase();
			}

			System.out.println("newQueueModel.getDomain_name()"
					+ newQueueModel.getDomain_name());

			newQueueModel.setQueueName(model.getQueue_name());
			newQueueModel.setIsActive("0");
			newQueueModel.setUpTo("0");
			newQueueModel.setSelecteddays("1 2 3 4 5 0 6");
			newQueueModel.setFrom("0");

			newQueueModel.setUserType(newQueueModel.getWhite_label());
			newQueueModel.setLiveRampPixelValue("");
			newQueueModel.setOpen_pixelCategory(newQueueModel.getWhite_label());
			newQueueModel.setSendGrid_ip_pool("");

			localList.add(newQueueModel);

		}

		Connection con = Utility.openConnection();
		java.sql.Statement st = null;
		try {
			if (con != null && !con.isClosed()) {

				st = con.createStatement();
			}
		} catch (Exception e) {
			// TODO: handle exception
		}

		for (int i = 0; i < localList.size(); i++) {

			QueueProcessModel newQueueModel = localList.get(i);
			System.out.println("domain=" + newQueueModel.getDomain_name());
			try {
				Utility.setSendGridMappingKey(newQueueModel);
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			System.out.println("getting email client="
					+ newQueueModel.getDomain_name()
					+ " ="
					+ Utility.domain_and_emailclient.get(newQueueModel
							.getDomain_name()));

			if (Utility.domain_and_emailclient.get(
					newQueueModel.getDomain_name()).equalsIgnoreCase("mailgun")) {

				Utility.setCampaignId(newQueueModel);

			} else if (Utility.domain_and_emailclient.get(
					newQueueModel.getDomain_name())
					.equalsIgnoreCase("sendgrid")) {

				Utility.setSendgridCategory(newQueueModel,
						Utility.domain_and_emailclient.get(newQueueModel
								.getDomain_name()));

			} else if (Utility.domain_and_emailclient.get(
					newQueueModel.getDomain_name()).equalsIgnoreCase(
					"sparkpost")) {
				Utility.setCampaignId(newQueueModel);
				System.out.println("comapgin id set !!");
				Utility.setSendgridCategory(newQueueModel,
						Utility.domain_and_emailclient.get(newQueueModel
								.getDomain_name()));

			}

			String user_process_lifecycle_dummy = "INSERT INTO "
					+ Utility.user_process_life_cycle_dummy
					+ ""
					+ " (`ProcessType`, `ProcessName`, `StartDay`, `EndDay`, `SelectedDays`, `DaysCount`, `isActive`, `upto`, `isRampup`, `today_rampup_status`, `ramup_type`, `queuename`, `query`, `dateCondition`, `showDashboard`, `Tablename`)"
					+ " VALUES ('" + newQueueModel.getProcessType() + "','"
					+ newQueueModel.getProcessName() + "', '"
					+ newQueueModel.getStartDay() + "', '"
					+ newQueueModel.getEndDay() + "', '"
					+ newQueueModel.getSelecteddays()
					+ "', '0', '1', '0', '', '', '', '"
					+ newQueueModel.getQueueName() + "', '"
					+ newQueueModel.getQuery() + "', '"
					+ newQueueModel.getDateCondition() + "', '1', '"
					+ newQueueModel.getTable_name() + "')";
			String white_label_process_queue = "INSERT INTO "
					+ Utility.white_label_queue_process
					+ " (`ProcessName`, `processType`, `PostalAddress`, `image_postaladdress`, `HtmlSourceName`, `ProviderName`, `T2ValueJ2c`, `JujuChannel`, `LiveRampPixels`, `CampgianId`, `domain_name`, `OpenPixelCategoryName`, `sendGridIpPool`, `SendGridCategoryName`, `sendgridMappingKey`, `subjectFileName`, `arborpixel`, `white_label`)"
					+ " VALUES ('" + newQueueModel.getProcessName() + "', '"
					+ newQueueModel.getProcessType() + "', '"
					+ newQueueModel.getPostalAddress() + "', '"
					+ newQueueModel.getImage_postaladdress() + "', '"
					+ newQueueModel.getUserType() + "', '"
					+ newQueueModel.getProcessName() + "', '"
					+ newQueueModel.getJ2cT2Value() + "', '"
					+ newQueueModel.getJujuChannelValue() + "', '"
					+ newQueueModel.getLiveRampPixelValue() + "', '"
					+ newQueueModel.getCampaign_id() + "', '"
					+ newQueueModel.getDomain_name() + "', '"
					+ newQueueModel.getOpen_pixelCategory() + "', '"
					+ newQueueModel.getSendGrid_ip_pool() + "', '"
					+ newQueueModel.getSendGridCategory() + "', '"
					+ newQueueModel.getSendgridMappingKey() + "', '"
					+ newQueueModel.getSubjectFileName() + "', '"
					+ newQueueModel.getArborpixel() + "', '"
					+ newQueueModel.getWhite_label() + "')";

			System.out.println(user_process_lifecycle_dummy);

			System.out.println(white_label_process_queue);

			if (model.getProcess_Type().equalsIgnoreCase("daily")) {
				if (i == 0) {
					try {
						boolean user_process_lifecycle_dummy_isexecute = st
								.execute(user_process_lifecycle_dummy);

						boolean white_label_process_queue_isexecute = st
								.execute(white_label_process_queue);

						if (!user_process_lifecycle_dummy_isexecute
								&& !white_label_process_queue_isexecute) {
							resultString = model.getWhitelable_name()
									+ " with processtype="
									+ model.getProcess_Type()
									+ " with Adervertisement="
									+ model.getAdvertisement()
									+ " have been successfully added";
							System.out
									.println("Successfully inserted in table");
						}
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				break;

			} else if (model.getProcess_Type().equalsIgnoreCase("opener")) {

				if (i == 1) {
					try {
						boolean user_process_lifecycle_dummy_isexecute = st
								.execute(user_process_lifecycle_dummy);

						boolean white_label_process_queue_isexecute = st
								.execute(white_label_process_queue);

						if (!user_process_lifecycle_dummy_isexecute
								&& !white_label_process_queue_isexecute) {

							resultString = model.getWhitelable_name()
									+ " with processtype="
									+ model.getProcess_Type()
									+ " with Adervertisement="
									+ model.getAdvertisement()
									+ " have been successfully added";
							System.out
									.println("Successfully inserted in table");
						}
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					break;

				}
			} else if (model.getProcess_Type().equalsIgnoreCase("clicker")) {

				if (i == 2) {
					try {
						boolean user_process_lifecycle_dummy_isexecute = st
								.execute(user_process_lifecycle_dummy);

						boolean white_label_process_queue_isexecute = st
								.execute(white_label_process_queue);

						if (!user_process_lifecycle_dummy_isexecute
								&& !white_label_process_queue_isexecute) {
							resultString = model.getWhitelable_name()
									+ " with processtype="
									+ model.getProcess_Type()
									+ " with Adervertisement="
									+ model.getAdvertisement()
									+ " have been successfully added";
							System.out
									.println("Successfully inserted in table");
						}
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					break;
				}

			} else {

				try {
					boolean user_process_lifecycle_dummy_isexecute = st
							.execute(user_process_lifecycle_dummy);

					boolean white_label_process_queue_isexecute = st
							.execute(white_label_process_queue);

					if (!user_process_lifecycle_dummy_isexecute
							&& !white_label_process_queue_isexecute) {
						resultString = model.getWhitelable_name()
								+ " with processtype="
								+ model.getProcess_Type()
								+ " with Adervertisement="
								+ model.getAdvertisement()
								+ " have been successfully added";
						System.out.println("Successfully inserted in table");
					}
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}

		}
		localList.clear();

		return resultString;
	}

	@Override
	public HashMap<String, ArrayList<Integer>> createDomainWiseCompaingHashMap() {
		// TODO Auto-generated method stub

		HashMap<String, ArrayList<Integer>> localHashMap = new HashMap<String, ArrayList<Integer>>();
		String query = "select domain_name,campgianid from "
				+ Utility.white_label_queue_process
				+ " where campgianid!='' group by 1,2";

		System.out.println(query);

		Connection con = Utility.openConnection();

		try {
			Statement st = con.createStatement();
			ResultSet res = st.executeQuery(query);
			ArrayList<Integer> list = null;
			while (!res.isClosed() && res.next()) {
				list = localHashMap.get(res.getString("domain_name").trim());

				if (list == null) {
					list = new ArrayList<Integer>();
				}
				list.add(Integer.parseInt(res.getString("campgianid")));

				localHashMap.put(res.getString("domain_name").trim(), list);

			}

			if (con != null) {
				con.close();
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return localHashMap;

	}

	@Override
	public ArrayList<Integer> create_sendGridMappingKey() {
		// TODO Auto-generated method stub

		String query = "select sendgridMappingKey from "
				+ Utility.white_label_queue_process + " group by 1";
		ArrayList<Integer> list = new ArrayList<Integer>();

		System.out.println(query);

		Connection con = Utility.openConnection();

		try {
			Statement st = con.createStatement();
			ResultSet res = st.executeQuery(query);
			while (!res.isClosed() && res.next()) {

				try {
					list.add(Integer.parseInt(res
							.getString("sendgridMappingKey")));
				} catch (NumberFormatException e) {
					// TODO Auto-generated catch block
					// e.printStackTrace();
				}

			}

			if (con != null) {
				con.close();
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return list;
	}

	@Override
	public HashMap<String, String> create_domain_andits_emailClient_map() {
		// TODO Auto-generated method stub

		String query = "select DomainUrl,EmailClient from whitelabels_process group by 1,2";
		HashMap<String, String> domain_and_emailclient = new HashMap<String, String>();

		System.out.println(query);

		Connection con = Utility.openConnection();

		try {
			Statement st = con.createStatement();
			ResultSet res = st.executeQuery(query);
			while (!res.isClosed() && res.next()) {

				if (res.getString("DomainUrl") != null
						&& res.getString("EmailClient") != null) {
					domain_and_emailclient.put(res.getString("DomainUrl")
							.trim(), res.getString("EmailClient").trim());
					System.out.println(res.getString("DomainUrl").trim()
							+ " email client="
							+ res.getString("EmailClient").trim());
				}

			}

			if (con != null) {
				con.close();
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return domain_and_emailclient;
	}

	@Override
	public HashMap<String, String> create_advertisement_info() {
		// TODO Auto-generated method stub

		String query = "select job_seeker,channel,postal_address,image_postaladdress,subject_fromname,arbor_pixel from tbl_seekers_wise_data";
		HashMap<String, String> advertisement_wise_info = new HashMap<String, String>();

		System.out.println(query);

		Connection con = Utility.openConnection();

		try {
			Statement st = con.createStatement();
			ResultSet res = st.executeQuery(query);
			while (!res.isClosed() && res.next()) {

				advertisement_wise_info.put(
						res.getString("job_seeker").toLowerCase().trim(),
						res.getString("channel") + "|"
								+ res.getString("postal_address") + "|"
								+ res.getString("image_postaladdress") + "|"
								+ res.getString("subject_fromname").trim()
								+ "|" + res.getString("arbor_pixel").trim());

				// System.out.println(res.getString("job_seeker").toLowerCase()
				// .trim()
				// + " values="
				// + res.getString("channel")
				// + "|"
				// + res.getString("postal_address")
				// + "|"
				// + res.getString("image_postaladdress")
				// + "|"
				// + res.getString("subject_fromname").trim()
				// + "|"
				// + res.getString("arbor_pixel").trim());

			}

			if (con != null) {
				con.close();
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return advertisement_wise_info;
	}

	@Override
	public String addNew_WebprocessInTable(ProcessModel model) {
		// TODO Auto-generated method stub

		// TODO Auto-generated method stub

		// in this we are creating first list of new model object with all types
		// then we iterate loop and insert the respective record

		if (model.getFresher_domain().contains("-")) {

			model.setFresher_domain(model.getFresher_domain().split("-")[0]);

		}
		if (model.getFresher_second_doman().contains("-")) {

			model.setFresher_second_doman(model.getFresher_second_doman()
					.split("-")[0]);

		}
		if (model.getOpener_domain().contains("-")) {

			model.setOpener_domain(model.getOpener_domain().split("-")[0]);

		}
		if (model.getWeekly_domain().contains("-")) {

			model.setWeekly_domain(model.getWeekly_domain().split("-")[0]);

		}

		String resultString = "";
		ArrayList<QueueProcessModel> localList = new ArrayList<QueueProcessModel>();

		System.out.println(model.getWhitelable_name() + "|"
				+ model.getProcess_Type() + "|" + model.getAdvertisement()
				+ "|" + model.getQueue_name() + "|");
		for (int i = 0; i < 5; i++) {
			QueueProcessModel newQueueModel = new QueueProcessModel();
			newQueueModel.setProcessName(model.getWhitelable_name());
			newQueueModel.setWhite_label(model.getWhitelable_name());
			newQueueModel.setTable_name(Utility.web_alerts_table);
			newQueueModel.setUserType(model.getWhitelable_name() + "-web");
			newQueueModel.setOpen_pixelCategory(model.getWhitelable_name());
			if (i == 0) {
				newQueueModel.setProcessType("daily-web");
				newQueueModel
						.setQuery("SELECT DISTINCT count(*) AS rowcount FROM "
								+ newQueueModel.getTable_name()
								+ " t1 left join tbl_zipcodes t2 on t1.Zip=t2.zip WHERE (LENGTH(KEYWORD)>2) "
								+ "AND ( t1.provider like \\'%"
								+ model.getWhitelable_name()
								+ "%\\'"
								+ "  ) AND frequency=\\'1\\' and (open=\\'0\\' and click=\\'0\\')");
				newQueueModel
						.setDateCondition("t1.DateTime > now()-interval # day");
				newQueueModel.setDomain_name(model.getFresher_domain());
				newQueueModel.setEndDay("14");

			}
			if (i == 1) {
				newQueueModel.setProcessType("opener-web");
				newQueueModel
						.setQuery("SELECT DISTINCT count(*) AS rowcount FROM "
								+ newQueueModel.getTable_name()
								+ " t1 left join tbl_zipcodes t2 on t1.Zip=t2.zip WHERE (LENGTH(KEYWORD)>2) "
								+ "AND ( t1.provider like \\'%"
								+ model.getWhitelable_name()
								+ "%\\'"
								+ "  ) AND frequency=\\'1\\' and (open=\\'1\\' and click=\\'0\\')");
				newQueueModel
						.setDateCondition("t1.open_at  > now()-interval # day");
				newQueueModel.setDomain_name(model.getOpener_domain());
				newQueueModel.setEndDay("14");

			}
			if (i == 2) {
				newQueueModel.setProcessType("clicker-web");
				newQueueModel
						.setQuery("SELECT DISTINCT count(*) AS rowcount FROM "
								+ newQueueModel.getTable_name()
								+ " t1 left join tbl_zipcodes t2 on t1.Zip=t2.zip WHERE (LENGTH(KEYWORD)>2) "
								+ "AND ( t1.provider like \\'%"
								+ model.getWhitelable_name()
								+ "%\\'"
								+ "  ) AND frequency=\\'1\\' and (open=\\'1\\' and click=\\'1\\')");
				newQueueModel
						.setDateCondition("t1.open_at > now()-interval # day");
				newQueueModel.setDomain_name(model.getOpener_domain());
				newQueueModel.setEndDay("14");

			}
			if (i == 3) {
				newQueueModel.setProcessType("weekly-web");
				newQueueModel
						.setQuery("SELECT DISTINCT count(*) AS rowcount FROM "
								+ newQueueModel.getTable_name()
								+ " t1 left join tbl_zipcodes t2 on t1.Zip=t2.zip WHERE (LENGTH(KEYWORD)>2) "
								+ "AND ( t1.provider like \\'%"
								+ model.getWhitelable_name() + "%\\'"
								+ "  ) AND frequency=\\'1\\'");
				newQueueModel
						.setDateCondition("t1.open_at  < now()-interval # day and t2.open_at  > now()-interval # day");
				newQueueModel.setDomain_name(model.getWeekly_domain());
				newQueueModel.setEndDay("6");

			}
			if (i == 4) {
				newQueueModel.setProcessType("monthly-web");
				newQueueModel
						.setQuery("SELECT DISTINCT count(*) AS rowcount FROM "
								+ newQueueModel.getTable_name()
								+ " t1 left join tbl_zipcodes t2 on t1.Zip=t2.zip WHERE (LENGTH(KEYWORD)>2) "
								+ "AND ( t1.provider like \\'%"
								+ model.getWhitelable_name() + "%\\'"
								+ "  ) AND frequency=\\'1\\'");
				newQueueModel
						.setDateCondition("t1.open_at  < now()-interval # day and t2.open_at  > now()-interval # day");
				newQueueModel.setDomain_name(model.getWeekly_domain());
				newQueueModel.setEndDay("3");

			}

			System.out.println("getting advertisement info fro adver="
					+ model.getAdvertisement());

			String dataArray[] = null;

			String data = Utility.advertisement_wise_info.get(model
					.getWhitelable_name().toLowerCase());
			if (data != null) {
				try {
					dataArray = data.split("\\|");
					newQueueModel.setJujuChannelValue(dataArray[0]);
					newQueueModel.setJ2cT2Value("&t2=" + dataArray[0]);
					newQueueModel.setPostalAddress(dataArray[1]);
					newQueueModel.setImage_postaladdress(dataArray[2]);
					newQueueModel.setSubjectFileName(dataArray[3]);
					newQueueModel.setArborpixel(dataArray[4]);
				} catch (Exception e) {
					// TODO Auto-generated catch block
				}
			} else {
				localList.clear();
				return "no advertisement data found for "
						+ model.getAdvertisement().toLowerCase();
			}

			newQueueModel.setQueueName(model.getQueue_name());
			newQueueModel.setIsActive("1");
			newQueueModel.setUpTo("0");
			newQueueModel.setSelecteddays("1 2 3 4 5 0 6");
			newQueueModel.setFrom("0");

			newQueueModel.setUserType(newQueueModel.getWhite_label());
			newQueueModel.setLiveRampPixelValue("");
			newQueueModel.setOpen_pixelCategory(newQueueModel.getWhite_label());
			newQueueModel.setSendGrid_ip_pool("");

			localList.add(newQueueModel);

		}

		Connection con = Utility.openConnection();
		java.sql.Statement st = null;
		try {
			if (con != null && !con.isClosed()) {

				st = con.createStatement();
			}
		} catch (Exception e) {
			// TODO: handle exception
		}

		for (int i = 0; i < localList.size(); i++) {

			QueueProcessModel newQueueModel = localList.get(i);
			System.out.println("domain=" + newQueueModel.getDomain_name());
			try {
				Utility.setSendGridMappingKey(newQueueModel);
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			System.out.println();
			if (Utility.domain_and_emailclient.get(
					newQueueModel.getDomain_name()).equalsIgnoreCase("mailgun")) {

				Utility.setCampaignId(newQueueModel);

			} else if (Utility.domain_and_emailclient.get(
					newQueueModel.getDomain_name())
					.equalsIgnoreCase("sendgrid")) {

				Utility.setSendgridCategory(newQueueModel,
						Utility.domain_and_emailclient.get(newQueueModel
								.getDomain_name()));

			} else if (Utility.domain_and_emailclient.get(
					newQueueModel.getDomain_name()).equalsIgnoreCase(
					"sparkpost")) {
				Utility.setCampaignId(newQueueModel);
				System.out.println("comapgin id set !!");
				Utility.setSendgridCategory(newQueueModel,
						Utility.domain_and_emailclient.get(newQueueModel
								.getDomain_name()));

			}

			String user_process_lifecycle_dummy = "INSERT INTO "
					+ Utility.user_process_life_cycle_dummy
					+ ""
					+ " (`ProcessType`, `ProcessName`, `StartDay`, `EndDay`, `SelectedDays`, `DaysCount`, `isActive`, `upto`, `isRampup`, `today_rampup_status`, `ramup_type`, `queuename`, `query`, `dateCondition`, `showDashboard`, `Tablename`)"
					+ " VALUES ('" + newQueueModel.getProcessType() + "','"
					+ newQueueModel.getProcessName() + "', '0', '"
					+ newQueueModel.getEndDay() + "', '"
					+ newQueueModel.getSelecteddays()
					+ "', '0', '0', '0', '', '', '', '"
					+ newQueueModel.getQueueName() + "','"
					+ newQueueModel.getQuery() + "', '"
					+ newQueueModel.getDateCondition() + "', '1', '"
					+ newQueueModel.getTable_name() + "')";
			String white_label_process_queue = "INSERT INTO "
					+ Utility.white_label_queue_process
					+ " (`ProcessName`, `processType`, `PostalAddress`, `image_postaladdress`, `HtmlSourceName`, `ProviderName`, `T2ValueJ2c`, `JujuChannel`, `LiveRampPixels`, `CampgianId`, `domain_name`, `OpenPixelCategoryName`, `sendGridIpPool`, `SendGridCategoryName`, `sendgridMappingKey`, `subjectFileName`, `arborpixel`, `white_label`)"
					+ " VALUES ('" + newQueueModel.getProcessName() + "', '"
					+ newQueueModel.getProcessType() + "', '"
					+ newQueueModel.getPostalAddress() + "', '"
					+ newQueueModel.getImage_postaladdress() + "', '"
					+ newQueueModel.getUserType() + "', '"
					+ newQueueModel.getProcessName() + "', '"
					+ newQueueModel.getJ2cT2Value() + "', '"
					+ newQueueModel.getJujuChannelValue() + "', '"
					+ newQueueModel.getLiveRampPixelValue() + "', '"
					+ newQueueModel.getCampaign_id() + "', '"
					+ newQueueModel.getDomain_name() + "', '"
					+ newQueueModel.getOpen_pixelCategory() + "', '"
					+ newQueueModel.getSendGrid_ip_pool() + "', '"
					+ newQueueModel.getSendGridCategory() + "', '"
					+ newQueueModel.getSendgridMappingKey() + "', '"
					+ newQueueModel.getSubjectFileName() + "', '"
					+ newQueueModel.getArborpixel() + "', '"
					+ newQueueModel.getWhite_label() + "')";

			System.out.println(user_process_lifecycle_dummy);

			System.out.println(white_label_process_queue);

			try {
				boolean user_process_lifecycle_dummy_isexecute = st
						.execute(user_process_lifecycle_dummy);

				boolean white_label_process_queue_isexecute = st
						.execute(white_label_process_queue);

				if (!user_process_lifecycle_dummy_isexecute
						&& !white_label_process_queue_isexecute) {
					resultString = model.getWhitelable_name()
							+ " with processtype=" + model.getProcess_Type()
							+ " with Adervertisement="
							+ model.getAdvertisement()
							+ " have been successfully added";
					System.out.println("Successfully inserted in table");
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		localList.clear();

		return resultString;

	}

	public HashMap<String, String> getWhitelable_table_map_with_types() {
		// TODO Auto-generated method stub
		HashMap<String, String> localHashMap = new HashMap<String, String>();
		// String query =
		// "select t1.domain_name,substring_index(t1.processType,'-','1')  as processType from white_label_queue_process t1 inner join user_process_lifecycle_dummy t2 on t1.processType=t2.processType where t1.processType not like '%web%' and t1.processName not like '%inactive%' and t1.processType !='' and t2.isActive='1' group by 1,2";
		String query = "select t1.domain_name, t1.processType from white_label_queue_process t1 inner join user_process_lifecycle_dummy t2 on t1.processType=t2.processType where t1.processType not like '%web%' and t1.processName not like '%inactive%' and t1.processType !='' and t2.isActive='1' group by 1,2 order by t1.id desc";

		System.out.println(query);

		Connection con = Utility.openConnection();

		try {
			if (con != null && !con.isClosed()) {

				java.sql.Statement st = con.createStatement();

				ResultSet res = st.executeQuery(query);

				while (!res.isClosed() && res.next()) {

					String localDomain_type = "";
					try {
						System.out.println(res.getString("domain_name"));

						String processType = res.getString("processType")
								.trim();
						if (processType.toLowerCase().contains("second")) {
							processType = "daily-second";
						} else if (processType.toLowerCase().contains("daily")) {
							processType = "daily";

						} else if (processType.toLowerCase()
								.contains("clicker")) {
							processType = "clicker";

						} else if (processType.toLowerCase().contains("opener")) {
							processType = "opener";
						} else if (processType.toLowerCase().contains("weekly")) {
							processType = "weekly";

						} else if (processType.toLowerCase()
								.contains("monthly")) {
							processType = "monthly";

						}
						localDomain_type = localHashMap.get(res.getString(
								"domain_name").trim());

						if (localDomain_type == null) {
							localHashMap.put(res.getString("domain_name")
									.trim(), processType);

						}
					} catch (Exception e) {
						// TODO Auto-generated catch block
					}

				}

			}
			if (con != null) {
				con.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return localHashMap;
	}

	@Override
	public ArrayList<String> get_all_advertisement_list() {
		// TODO Auto-generated method stub

		String query = "SELECT distinct job_seeker as job_seeker from tbl_seekers_wise_data T1 WHERE NOT EXISTS (SELECT whitelabel_name FROM whitelabels_process T2 WHERE T1.job_seeker = T2.whitelabel_name)";
		ArrayList<String> advertisement_list = new ArrayList<String>();
		System.out.println(query);

		Connection con = Utility.openConnection();

		try {
			Statement st = con.createStatement();
			ResultSet res = st.executeQuery(query);
			while (!res.isClosed() && res.next()) {
				advertisement_list.add(res.getString("job_seeker")
						.toLowerCase());
			}

			if (con != null) {
				con.close();
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return advertisement_list;
	}

	@Override
	public List<SearchResultModel> get_search_resultSetFor_advt(
			AdvertisementModel model) {
		// TODO Auto-generated method stub

		List<SearchResultModel> searchResult_model_list = new ArrayList<SearchResultModel>();

		System.out.println(model.getFromDateSelected() + " and t-="
				+ model.getToDateSelected() + "whitelable=" + model.getSelectedWhitelable());
		String query = "";

		Connection con = Utility.openConnection();
		List<String> all_whitelable_with_advertisement = null;
		if (model.getSelectedWhitelable().equalsIgnoreCase("all")) {
			all_whitelable_with_advertisement = model.getAdvertisement_wise_whitelables_list().get(model.getSelectedAdvertisement());

		} else {
			all_whitelable_with_advertisement = new ArrayList<String>();
			all_whitelable_with_advertisement.add(model.getSelectedWhitelable());
		}

		for (int whilable_index = 0; whilable_index < all_whitelable_with_advertisement.size(); whilable_index++) {
			model.setSelectedWhitelable(all_whitelable_with_advertisement.get(whilable_index));

			SearchResultModel searchResultModel = new SearchResultModel();
			searchResultModel.setDataInterValList(Utility.createDataInterval(
					model.getFromDateSelected(), model.getToDateSelected()));
			searchResultModel.setWhitelabelName(model.getSelectedWhitelable());

			for (int i = 0; i < 4; i++) {
				String open_click_condition = "open='0' and click='0'";

				// register
				if (i == 0) {

					query = "SELECT count(*) as count ,date(date) as date from "
							+ model.getWhitelable_table_map().get(model.getSelectedWhitelable())
							+ " where date(date) <='" + model.getToDateSelected()
							+ "'" + " and date(date)>='"
							+ model.getFromDateSelected() + "' and advertisement='"
							+ model.getSelectedAdvertisement()
							+ "' group by 2 order by 2 asc";
				}
				// fresher
				else if (i == 1) {
					if (model.getSelectedWhitelable().toLowerCase()
							.contains("cylcon")) {
						open_click_condition = "open='0' and clicked='0'";

					} else {
						open_click_condition = "open='0' and click='0'";
					}
					query = "SELECT count(*) as count ,date(date) as date from "
							+ model.getWhitelable_table_map().get(
									model.getSelectedWhitelable())
							+ " where date(date) <='" + model.getToDateSelected()
							+ "'" + " and date(date)>='"
							+ model.getFromDateSelected() + "' and advertisement='"
							+ model.getSelectedAdvertisement() + "' and "
							+ open_click_condition + " group by 2 order by 2 asc";

				}
				// opener
				else if (i == 2) {
					if (model.getSelectedWhitelable().toLowerCase()
							.contains("cylcon")) {
						open_click_condition = "open='1' and clicked='0'";

					} else {
						open_click_condition = "open='1' and click='0'";
					}
					query = "SELECT count(*) as count ,date(date) as date from "
							+ model.getWhitelable_table_map().get(
									model.getSelectedWhitelable())
							+ " where date(date) <='" + model.getToDateSelected()
							+ "'" + " and date(date)>='"
							+ model.getFromDateSelected() + "' and advertisement='"
							+ model.getSelectedAdvertisement() + "' and "
							+ open_click_condition + " group by 2 order by 2 asc";

				}
				// clicker
				else {
					if (model.getSelectedWhitelable().toLowerCase()
							.contains("cylcon")) {
						open_click_condition = "open='1' and clicked='1'";

					} else {
						open_click_condition = "open='1' and click='1'";
					}

					query = "SELECT count(*) as count ,date(date) as date from "
							+ model.getWhitelable_table_map().get(
									model.getSelectedWhitelable())
							+ " where date(date) <='" + model.getToDateSelected()
							+ "'" + " and date(date)>='"
							+ model.getFromDateSelected() + "' and advertisement='"
							+ model.getSelectedAdvertisement() + "' and "
							+ open_click_condition + " group by 2 order by 2 asc";

				}
				try {
					System.out.println(query);
					Statement st = con.createStatement();
					ResultSet res = st.executeQuery(query);
					while (!res.isClosed() && res.next()) {
						if (i == 0) {
							searchResultModel.rampup_register_users.put(
									res.getString("date"),
									Integer.parseInt(res.getString("count")));

						} else if (i == 1) {

							searchResultModel.rampup_freshers_users.put(
									res.getString("date"),
									Integer.parseInt(res.getString("count")));

						} else if (i == 2) {

							searchResultModel.rampup_opener_users.put(
									res.getString("date"),
									Integer.parseInt(res.getString("count")));

						} else {

							searchResultModel.rampup_clicker_users.put(
									res.getString("date"),
									Integer.parseInt(res.getString("count")));

						}

					}

				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();

				}

			}

			searchResult_model_list.add(searchResultModel);
		}

		try {
			if (con != null) {
				con.close();
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// System.out.println(searchResultModel.rampup_clicker_users.size());
		// System.out.println(searchResultModel.rampup_freshers_users.size());
		// System.out.println(searchResultModel.rampup_opener_users.size());

		return searchResult_model_list;
	}

	@Override
	public ProviderToMainProviderModel getProvideToMainProviderModel() {
		// TODO Auto-generated method stub

		ProviderToMainProviderModel model = new ProviderToMainProviderModel();
		BufferedReader bufferReader = null;
		FileReader fileReader = null;

		try {

			// br = new BufferedReader(new FileReader(FILENAME));
			fileReader = new FileReader(Utility.provider_to_mainProvider_file);
			bufferReader = new BufferedReader(fileReader);

			String sCurrentLine;

			while ((sCurrentLine = bufferReader.readLine()) != null) {
				System.out.println(sCurrentLine);
				String provierArray[] = sCurrentLine.split("\\|");
				if (provierArray != null && provierArray.length > 1) {

					model.providerList.add(provierArray[0]);
					model.main_providerList.add(provierArray[1]);

					model.provider_to_main_provider_map.put(provierArray[0],
							provierArray[1]);

					model.main_provider_to_provider_map.put(provierArray[1],
							provierArray[0]);
					ArrayList<String> lastList = model.main_provider_to_provider_list_map
							.get(provierArray[1]);

					if (lastList == null) {
						lastList = new ArrayList<String>();
					}

					lastList.add(provierArray[0]);

					model.main_provider_to_provider_list_map.put(
							provierArray[1], lastList);

				}

			}

		} catch (IOException e) {

			e.printStackTrace();

		} finally {

			try {

				if (bufferReader != null)
					bufferReader.close();

				if (fileReader != null)
					fileReader.close();

			} catch (IOException ex) {

				ex.printStackTrace();

			}

		}

		return model;
	}

	@Override
	public ProviderBaseModel writeDataInProviderFile(
			ProviderBaseModel providerMainModel) {
		// TODO Auto-generated method stub
		System.out.println(" write method size of Utility.last_provider_list=" + Utility.last_provider_list.size());
		try {
			FileWriter fw = new FileWriter(
					Utility.provider_to_mainProvider_file, true); // the true
																	// will
			// append the new
			// data
			fw.write(providerMainModel.getProviderName() + "|"
					+ providerMainModel.getDisplayProviderName() + "\n");// appends
																			// the
			// string
			// to the
			// file
			System.out.println("file updated success fully");
			fw.close();

			String change_css = "style='color: red;background: papayawhip;'";

			String html = "<html><table border='2'><tr><th style='text-align: center;'>Provider Name</th><th style='text-align: center;'>Provider Display Value</th></tr>";
			for (int i = 0; i < Utility.last_provider_list.size(); i++) {
				String default_css = "";

				html = html + "<tr><td>" + Utility.last_provider_list.get(i).getProviderName() + "</td><td " + default_css + ">" + Utility.last_provider_list.get(i).getDisplayProviderName()
						+ "</td></tr>";
			}
			html = html + "<tr><td " + change_css + ">" + providerMainModel.getProviderName() + "</td><td " + change_css + ">" + providerMainModel.getDisplayProviderName()
					+ "</td></tr>";

			html = html + "<table></html>";
			Utility.updated_html = html;
			Utility.last_provider_list.add(providerMainModel);

		} catch (IOException ioe) {
			System.err.println("IOException: " + ioe.getMessage());
		}
		return providerMainModel;
	}

	@Override
	public NewSeekersModelData getComplete_info_of_seekers() {
		// TODO Auto-generated method stub

		String query = "SELECT * from tbl_seekers_wise_data ";
		NewSeekersModelData newSeekerModel = new NewSeekersModelData();
		ArrayList<SeekersModel> advertisement_list = new ArrayList<SeekersModel>();
		Utility.seekerList.clear();
		Utility.channelList.clear();
		Utility.postal_addressList.clear();
		Utility.image_postal_addressList.clear();
		Utility.subjectAndFromNameList.clear();
		Utility.arbor_pixelList.clear();

		System.out.println(query);

		Connection con = Utility.openConnection();

		try {

			Statement st = con.createStatement();
			ResultSet res = st.executeQuery(query);
			List<SeekersModel> list = new ArrayList<SeekersModel>();

			while (!res.isClosed() && res.next()) {
				SeekersModel model = newSeekerModel.getSeekers_map().get(res.getString("job_seeker"));

				if (model == null) {
					model = new SeekersModel();
					model.setSeekerName(res.getString("job_seeker"));
					model.setChannel(res.getString("channel"));
					model.setPostal_address(res.getString("postal_address"));
					model.setImage_postal_address(res.getString("image_postaladdress"));
					model.setSubjectAndFromName(res.getString("subject_fromname"));
					model.setArbor_pixel(res.getString("arbor_pixel"));

				}

				list.add(model);
				newSeekerModel.getSeekers_map().put(res.getString("job_seeker"), model);

				Utility.seekerList.add(model.getSeekerName());
				Utility.channelList.add(model.getChannel());
				Utility.postal_addressList.add(model.getPostal_address());
				Utility.image_postal_addressList.add(model.getImage_postal_address());
				Utility.subjectAndFromNameList.add(model.getSubjectAndFromName());
				Utility.arbor_pixelList.add(model.getArbor_pixel());

				//
				// advertisement_list.add(model);

			}
			newSeekerModel.setSeekersList(list);

			for (int i = 0; i < newSeekerModel.getSeekersList().size(); i++) {

				newSeekerModel.getSeekersList().get(i).setSeekerList(Utility.seekerList);
				newSeekerModel.getSeekersList().get(i).setChannelList(Utility.channelList);
				newSeekerModel.getSeekersList().get(i).setPostal_addressList(Utility.postal_addressList);
				newSeekerModel.getSeekersList().get(i).setImage_postal_addressList(Utility.image_postal_addressList);
				newSeekerModel.getSeekersList().get(i).setSubjectAndFromNameList(Utility.subjectAndFromNameList);
				newSeekerModel.getSeekersList().get(i).setArbor_pixelList(Utility.arbor_pixelList);
			}

			if (con != null) {
				con.close();
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return newSeekerModel;
	}

	@Override
	public SeekersModel add_new_seekers_into_database(SeekersModel model) {
		// TODO Auto-generated method stub

		return null;
	}

	@Override
	public List<ProviderBaseModel> getListOfProviderAndMainProvier() {
		// TODO Auto-generated method stub

		// TODO Auto-generated method stub
		Utility.last_provider_list.clear();

		List<ProviderBaseModel> list = new ArrayList<ProviderBaseModel>();
		BufferedReader bufferReader = null;
		FileReader fileReader = null;

		try {

			// br = new BufferedReader(new FileReader(FILENAME));
			fileReader = new FileReader(Utility.provider_to_mainProvider_file);
			bufferReader = new BufferedReader(fileReader);

			String sCurrentLine;
			int position = 0;
			while ((sCurrentLine = bufferReader.readLine()) != null) {
				System.out.println(sCurrentLine);
				String provierArray[] = sCurrentLine.split("\\|");
				if (provierArray != null && provierArray.length > 1) {
					ProviderBaseModel baseModel = new ProviderBaseModel();
					baseModel.setProviderName(provierArray[0]);
					baseModel.setDisplayProviderName(provierArray[1]);
					baseModel.setPosition(position);
					baseModel.setUpdate(false);
					list.add(baseModel);
					Utility.last_provider_list.add(baseModel);

					position++;

				}

			}
			System.out.println("size od last list=" + Utility.last_provider_list.size());

		} catch (IOException e) {

			e.printStackTrace();

		} finally {

			try {

				if (bufferReader != null)
					bufferReader.close();

				if (fileReader != null)
					fileReader.close();

			} catch (IOException ex) {

				ex.printStackTrace();

			}

		}

		return list;

	}

	@Override
	public List<ProviderBaseModel> updateProviderList_with_updatedvalue(List<ProviderBaseModel> list) {
		// TODO Auto-generated method stub
		System.out.println(" update method size of Utility.last_provider_list=" + Utility.last_provider_list.size());

		System.out.println("file going to update");
		FileOutputStream fooStream;
		String newContent = "";
		String change_css = "style='color: red;background: papayawhip;'";

		String html = "<html><table border='2'><tr><th style='text-align: center;'>Provider Name</th><th style='text-align: center;'>Old Display Value</th><th style='text-align: center;'>Updated Display Value</th></tr>";

		try {
			for (int i = 0; i < list.size(); i++) {
				String default_css = "";
				if (list.get(i).isUpdate()) {

					default_css = change_css;

				}
				html = html + "<tr><td>" + list.get(i).getProviderName() + "</td><td " + default_css + ">" + Utility.last_provider_list.get(i).getDisplayProviderName() + "</td><td " + default_css
						+ ">"
						+ list.get(i).getDisplayProviderName()
						+ "</td></tr>";
				list.get(i).setUpdate(false);
				newContent = newContent + list.get(i).getProviderName() + "|" + list.get(i).getDisplayProviderName() + "\n";
			}
			File myFoo = new File(Utility.provider_to_mainProvider_file);
			fooStream = new FileOutputStream(myFoo, false);
			byte[] myBytes = newContent.getBytes();
			fooStream.write(myBytes);
			fooStream.close();
			html = html + "<table></html>";
			Utility.updated_html = html;
			Utility.last_provider_list.clear();
			Utility.last_provider_list.addAll(list);
			return list;
		} catch (Exception e) {
			// TODO Auto-generated catch block

			// in this we can return that seesio has been expired so please
			// refresh page
			e.printStackTrace();
		} // true to append
			// false to overwrite.

		return null;
	}
}
