package com.project.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.projectmodel.AdvertisementModel;
import com.projectmodel.NewSeekersModelData;
import com.projectmodel.ProcessModel;
import com.projectmodel.ProviderBaseModel;
import com.projectmodel.ProviderToMainProviderModel;
import com.projectmodel.SearchResultModel;
import com.projectmodel.SeekersModel;

public interface ServicesInterface {

	public HashMap<String, String> getWhitelable_table_map();

	public List<String> get_new_rampup_advertisement();

	public ArrayList<String> getWhitelabesList(HashMap<String, String> map);

	public ArrayList<String> getQueueNamesList();

	public HashMap<String, ArrayList<String>> getWhitelabelWith_domain();

	public String addNewProcessInTable(ProcessModel model);

	public HashMap<String, ArrayList<Integer>> createDomainWiseCompaingHashMap();

	public ArrayList<Integer> create_sendGridMappingKey();

	public HashMap<String, String> create_domain_andits_emailClient_map();

	public HashMap<String, String> create_advertisement_info();

	public String addNew_WebprocessInTable(ProcessModel model);

	public HashMap<String, String> getWhitelable_table_map_with_types();

	ArrayList<String> get_all_advertisement_list();

	public List<SearchResultModel> get_search_resultSetFor_advt(
			AdvertisementModel model);

	public ProviderToMainProviderModel getProvideToMainProviderModel();

	ProviderBaseModel writeDataInProviderFile(
			ProviderBaseModel providerMainModel);

	NewSeekersModelData getComplete_info_of_seekers();

	SeekersModel add_new_seekers_into_database(SeekersModel model);

	public List<ProviderBaseModel> getListOfProviderAndMainProvier();

	public List<ProviderBaseModel> updateProviderList_with_updatedvalue(List<ProviderBaseModel> list);

}
