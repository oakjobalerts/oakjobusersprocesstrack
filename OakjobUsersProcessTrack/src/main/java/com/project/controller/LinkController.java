package com.project.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.project.service.impl.ServiceInterfaceImplementation;
import com.project.utility.Utility;
import com.projectmodel.AdvertisementModel;
import com.projectmodel.NewSeekersModelData;
import com.projectmodel.ProcessModel;
import com.projectmodel.ProviderBaseModel;
import com.projectmodel.ProviderToMainProviderModel;
import com.projectmodel.SearchResultModel;
import com.projectmodel.SeekersModel;
import com.projectmodel.UserModel;

@Controller
@RequestMapping("/")
public class LinkController {
	HttpSession session;

	@Autowired
	@RequestMapping(value = "/")
	public String editAlert() {
		System.out.println("in index Alert");
		// return "process_setup";
		// return "provider_to_mainprovider";
		return "login";
	}

	@RequestMapping(value = "/login")
	public String login_page() {
		System.out.println("in index Alert");
		return "login";
	}

	@RequestMapping(value = "/user_login", method = RequestMethod.POST)
	public @ResponseBody String user_login(@RequestBody UserModel model, HttpServletResponse response, HttpServletRequest req

			) {
		// return "process_setup";
		// return "provider_to_mainprovider";
		System.out.println("model.getUserName()" + model.getUserName() + "and model.getUserPassword()" + model.getUserPassword());
		if (model.getUserName().equalsIgnoreCase(Utility.userName) &&
				model.getUserPassword().equalsIgnoreCase(Utility.passWord)) {
			session = req.getSession();
			System.out.println("success");
			session.setAttribute("userName", "fbg-user");
			return "true";
		}
		else if (model.getUserName().equalsIgnoreCase(Utility.internal_userName) &&
				model.getUserPassword().equalsIgnoreCase(Utility.internal_passWord))
		{

			session = req.getSession();
			System.out.println("success");
			session.setAttribute("userName", "test");
			return "internal";
		}
		return "false";
	}

	@RequestMapping(value = "/all_important_links")
	public String all_important_links(HttpServletResponse response, HttpServletRequest req) {
		// return "process_setup";
		if (session != null && session.getAttribute("userName").equals("test"))
			return "all_important_links";
		else
			return "login";
	}

	@RequestMapping(value = "/process_setup")
	public String homePage(HttpServletResponse response, HttpServletRequest req) {
		System.out.println("in home page");

		if (session != null && session.getAttribute("userName").equals("test"))
			return "process_setup";
		else
			return "login";

	}

	@RequestMapping(value = "/advertisement_wise_data")
	public String advertisement_wise_data(HttpServletResponse response, HttpServletRequest req) {

		if (session != null && session.getAttribute("userName").equals("test"))
			return "advertisement_wise_data";
		else
			return "login";

	}

	@RequestMapping(value = "/chartGraph_providers")
	public String provider_to_mainprovider(HttpServletResponse response, HttpServletRequest request) {

		Utility.cc_mail_list.clear();
		Utility.cc_mail_list.add("rajinder@signitysolutions.com");
		Utility.cc_mail_list.add("nitika.chadha@signitysolutions.in");
		Utility.cc_mail_list.add("navneet@signitysolutions.in");
		Utility.cc_mail_list.add("vijayta.s@signitysolutions.in");
		Utility.cc_mail_list.add("parveen.k@signitysolutions.in");
		Utility.cc_mail_list.add("kanav@signitysolutions.com");

		System.out.println("successfuuly login");
		return "provider_to_mainprovider";

	}

	@RequestMapping(value = "/all_seekers_info")
	public String all_seekers_info(HttpServletResponse response, HttpServletRequest request) {

		if (session != null && session.getAttribute("userName").equals("test"))
			return "all_seekers_info";
		else
			return "login";

	}

	@RequestMapping(value = "/processData", method = RequestMethod.GET)
	public @ResponseBody ProcessModel checkUserCredentials() {

		System.out.println("in the process getting data");
		// here we will initialize the data for the process
		ProcessModel processModel = new ProcessModel();
		ServiceInterfaceImplementation service = new ServiceInterfaceImplementation();
		processModel.setAdvertiseMents(service.get_new_rampup_advertisement());
		processModel.setWhitelable_table_map(service.getWhitelable_table_map());
		processModel.setWhitelabelList(service.getWhitelabesList(processModel
				.getWhitelable_table_map()));
		processModel.setQueueNames(service.getQueueNamesList());

		processModel.setWhitelable_domainHashMap(service
				.getWhitelabelWith_domain());

		processModel.setWhitelable_domain_with_type(service
				.getWhitelable_table_map_with_types());

		Utility.domain_CompaignWise_Map = service
				.createDomainWiseCompaingHashMap();

		Utility.advertisement_wise_info = service.create_advertisement_info();
		Utility.domain_and_emailclient = service
				.create_domain_andits_emailClient_map();
		Utility.sendgridMappingKey = service.create_sendGridMappingKey();

		return processModel;
	}

	@RequestMapping(value = "/addNewProcess", method = RequestMethod.POST)
	public @ResponseBody String addNewProcess(@RequestBody ProcessModel model

			) {

		System.out.println("=============================//================"
				+ model.getWhitelable_name() + "|" + model.getAdvertisement());
		ServiceInterfaceImplementation service = new ServiceInterfaceImplementation();
		// here we are decinding whther web or other
		if (!model.getProcess_Type().equalsIgnoreCase("web"))
			return service.addNewProcessInTable(model);
		else
			return service.addNew_WebprocessInTable(model);

	}

	@RequestMapping(value = "/advertisement_info")
	public @ResponseBody AdvertisementModel advertisement_info() {
		ServiceInterfaceImplementation service = new ServiceInterfaceImplementation();

		AdvertisementModel advertisementModel = new AdvertisementModel();
		advertisementModel.setWhitelable_table_map(service
				.getWhitelable_table_map());
		advertisementModel.setWhitelable_table_map(service
				.getWhitelable_table_map());
		advertisementModel.setAdvertisements(service
				.get_all_advertisement_list());
		advertisementModel
				.setWhitelables(service.getWhitelabesList(advertisementModel
						.getWhitelable_table_map()));

		advertisementModel.setSelectedWhitelable(advertisementModel
				.getWhitelables().get(0));
		advertisementModel.setSelectedAdvertisement(advertisementModel
				.getAdvertisements().get(0));

		advertisementModel.setMyJsonModel(Utility
				.create_My_Jason_Model(Utility.filePath));

		advertisementModel = Utility
				.create_map_advertisement_with_whitelable(advertisementModel);

		System.out.println("in advertisement_info ");
		return advertisementModel;
	}

	@RequestMapping(value = "/search_result", method = RequestMethod.POST)
	public @ResponseBody List<SearchResultModel> search_result_for_advt(
			@RequestBody AdvertisementModel model

			) {
		ServiceInterfaceImplementation service = new ServiceInterfaceImplementation();
		SearchResultModel searchResultModel = new SearchResultModel();

		return service.get_search_resultSetFor_advt(model);

	}

	@RequestMapping(value = "/provider_to_mainprovider_info", method = RequestMethod.GET)
	public @ResponseBody List<ProviderBaseModel> getInformation_of_provider_conversion() {

		ProviderToMainProviderModel providerMainModel = new ProviderToMainProviderModel();

		ServiceInterfaceImplementation service = new ServiceInterfaceImplementation();
		List<ProviderBaseModel> list = service.getListOfProviderAndMainProvier();

		return list;
	}

	@RequestMapping(value = "/add_new_provider_to_mainProvider", method = RequestMethod.POST)
	public @ResponseBody ProviderBaseModel add_new_provider_main_provider(
			@RequestBody ProviderBaseModel model

			) {

		ServiceInterfaceImplementation service = new ServiceInterfaceImplementation();

		ProviderBaseModel newProviderModel = service.writeDataInProviderFile(model);
		Utility.sendSmtpEmail("New Seeker Is Added In SeekersMapping!!!", Utility.updated_html);
		return newProviderModel;

	}

	@Autowired
	@RequestMapping(value = "/seekers_info", method = RequestMethod.GET)
	public @ResponseBody NewSeekersModelData getCompleteInfo_Of_seekers() {

		ServiceInterfaceImplementation service = new ServiceInterfaceImplementation();

		return service.getComplete_info_of_seekers();

	}

	@RequestMapping(value = "/add_new_seekers_info", method = RequestMethod.POST)
	public @ResponseBody SeekersModel add_new_job_sekers_in_table(
			@RequestBody SeekersModel model
			) {

		ServiceInterfaceImplementation service = new ServiceInterfaceImplementation();

		return service.add_new_seekers_into_database(model);

	}

	@RequestMapping(value = "/update_provider_list", method = RequestMethod.POST)
	public @ResponseBody List<ProviderBaseModel> update_providers_list(@RequestBody List<ProviderBaseModel> list

			) {
		Utility.updated_html = "";
		ServiceInterfaceImplementation service = new ServiceInterfaceImplementation();
		List<ProviderBaseModel> new_list = service.updateProviderList_with_updatedvalue(list);
		Utility.sendSmtpEmail("Seekers Mapping has been Updated!!!", Utility.updated_html);

		return new_list;

	}
}
