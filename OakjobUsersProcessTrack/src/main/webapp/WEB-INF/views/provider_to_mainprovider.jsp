<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%
	session = request.getSession(false);

	if (session != null && session.getAttribute("userName") != null
			&& session.getAttribute("userName").equals("fbg-user")) {
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- <script
	src="http://cdn.jsdelivr.net/g/jquery@1,jquery.ui@1.10%28jquery.ui.core.min.js+jquery.ui.widget.min.js+jquery.ui.mouse.min.js+jquery.ui.sortable.min.js%29,angularjs@1.2,angular.ui-sortable"></script>

 -->
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<!-- <script
	src="http://ajax.googleapis.com/ajax/libs/angularjs/1.2.26/angular.min.js"></script>

 -->

<script
	src="//cdnjs.cloudflare.com/ajax/libs/angular-ui-bootstrap/0.11.0/ui-bootstrap-tpls.min.js"></script>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<link rel="stylesheet"
	href="//netdna.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">


<script
	src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.5/angular.min.js"></script>

<title>Provider_to_Mainprovider</title>


<link rel="stylesheet"
	href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

<link
	href="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css"
	rel="stylesheet">
<script language="javascript"
	src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.7/angular.js"></script>
<script language="javascript"
	src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.7/angular-animate.js"></script>
<script language="javascript"
	src="http://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.14.3.js"></script>

<script type="text/javascript"
	src="resources/js/provider_to_mainprovider.js"></script>
<link rel="stylesheet" href="resources/css/provider_mainprovider.css"
	type="text/css" />
<link
	href="${pageContext.servletContext.contextPath}/resources/css/xeditable.css"
	rel="stylesheet">
<script
	src="${pageContext.servletContext.contextPath}/resources/js/xeditable.js"></script>

</head>

<body style="Padding: 20px;" y>

	<h1 class="header">CHARTGRAPH SEEKERS MAPPING</h1>

	<div ng-app="provider_to_main_module"
		ng-controller="provider_controller"
		ng-init="initializer('${pageContext.servletContext.contextPath}')"
		style="text-align: center; background: #fbfafa;">

		<!-- <p style="margin-top: 103px; display: inline-block; width: 100%;">
			<a
				href="http://localhost:8080/OakjobUsersProcessTrack/all_important_links"
				target="#">Click Here for all important links</a>
		</p> -->


		<div class="modal-body row">
			<div class="form_div">
				<div class="col-md-12">
					<div class="col-md-6">
						<img src="http://oakjobalerts.com/img/logo.png"
							alt="Mountain View" style="width: 150px; height: 150px;">
					</div>
					<div class="col-md-6" style="margin-top: 41px;">
						<button class="button btn btn-success btn-lg" id="addnew"
							ng-click="show_add_dialog()">Add New</button>
						<button class="button btn btn-success btn-lg" id="addnew"
							ng-click="updateProviderList(allProviderList)">Update</button>
					</div>
				</div>
				<div class="clearfix"></div>


				<div class="table_div table-wrapper">
					<table class="tableClass table table-striped providerInfo">
						<tr>
							<th style="text-align: center;">UniqueProvider Name</th>
							<th style="text-align: center;">Display Name</th>

						</tr>
						<tr ng-repeat="providerModel in allProviderList">
							<td>{{ providerModel.providerName }}</td>
							<!-- <td>{{ providerModel.displayProviderName }}</td> -->
							<td><a href="#"
								editable-text="providerModel.displayProviderName"
								e-ng-change="change_updating_parameter(providerModel)">{{
									providerModel.displayProviderName || 'empty' }}</a></td>




						</tr>
					</table>
				</div>
			</div>



		</div>

		<!-- Modal -->
		<div id="myModal" class="modal fade" role="dialog">
			<div class="modal-dialog" style="width: 454px;">

				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title"
							style="font-size: x-large; color: darkgreen;">Add New
							Provider</h4>
					</div>
					<div class="modal-body">
						<div class="col-md-12">
							<div class="col-md-6">
								<input type="text" class="form-control" id="new_provider_name"
									placeholder="Enter unique provider name"
									style="width: 184px; margin-top: 20px;"
									ng-model="newProviderModel.providerName">
							</div>
							<div class="col-md-6">
								<input type="text" class="form-control"
									id="new_display_provider_name" placeholder="Enter Display name"
									style="width: 184px; margin-top: 20px;"
									ng-model="newProviderModel.displayProviderName">
							</div>
							<div class="clearfix"></div>

						</div>
						<div class="clearfix"></div>
						<div class="col-md-12" style="margin-top: 54px;">

							<button class="button btn btn-success btn-lg" id="add"
								ng-click="addNew_provider_Record(newProviderModel)">Click
								Here</button>


						</div>
						<div class="clearfix"></div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default"
								data-dismiss="modal">Close</button>
						</div>
					</div>

				</div>
			</div>



		</div>
		<div id="startpageLoadingGif" class="modal fade" role="dialog">
			<div class="modal-dialog"
				style="width: 150px; height: 150px; margin-top: 20px">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-body" style="">
						<div class="row" style="text-align: center;">
							<label> Please wait we are processing ! </label> <img
								src="${pageContext.servletContext.contextPath}/resources/images/startpageLoadingGif.gif" />
						</div>
					</div>
				</div>
			</div>
		</div>

		<div id="responsealert" class="modal fade" role="dialog">
			<div class="modal-dialog modal-sm" style="margin-top: 100px">
				<!-- Modal content-->
				<div class="modal-content">

					<div class="modal-body" style="">
						<div class="row" style="text-align: center;">
							<label> {{responseData}} </label>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>



</body>
</html>
<%
	} else {
		response.sendRedirect(request.getContextPath() + "/login");
	}
%>