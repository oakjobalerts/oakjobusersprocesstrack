<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<script
	src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.23/jquery-ui.min.js"></script>

<script
	src="http://cdn.jsdelivr.net/g/jquery@1,jquery.ui@1.10%28jquery.ui.core.min.js+jquery.ui.widget.min.js+jquery.ui.mouse.min.js+jquery.ui.sortable.min.js%29,angularjs@1.2,angular.ui-sortable"></script>

<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script
	src="http://ajax.googleapis.com/ajax/libs/angularjs/1.2.26/angular.min.js"></script>
<script
	src="//cdnjs.cloudflare.com/ajax/libs/angular-ui-bootstrap/0.11.0/ui-bootstrap-tpls.min.js"></script>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<link rel="stylesheet"
	href="//netdna.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">

<link
	href="${pageContext.servletContext.contextPath}/resources/css/login.css"
	rel="stylesheet">

<link
	href="${pageContext.servletContext.contextPath}/resources/css/index.css"
	rel="stylesheet">


<script
	src="${pageContext.servletContext.contextPath}/resources/js/login.js"></script>

<title>Login</title>
</head>
<body style="background-color: #e2e6de;">

	<div class="container">

		<div class="form-block login">

			<div ng-app="loginApp"
				ng-init="initilizePathVariable('${pageContext.servletContext.contextPath}')"
				ng-controller="loginController">

				<h3 class="my-login-text">
					<img src="http://oakjobalerts.com/img/logo.png">
				</h3>
				<div class="col-md-6 col-md-offset-3">
					<div class="user-login"id "login-div">
						<div class="form-group">
							<input class="form-control" id="userName" type="text"
								ng-model="userModel.userName" placeholder="Enter Username">
						</div>
						<div class="form-group">
							<input class="form-control" id="userPassword" type="password"
								ng-model="userModel.userPassword" placeholder="Enter Password">
						</div>
						<div class="form-group">
							<script>
								$("input").keypress(function(event) {
									if (event.which == 13) {
										$("#submit").click();
									}
								});
							</script>
							<form>

								<button class="button btn btn-success btn-lg" id="submit"
									ng-click="loginuser(userModel)">LOGIN</button>
							</form>
						</div>

						<label id="invalid_credential_label" class="ivalid-user-text"
							ng-show="isUserInValid">You have entered wrong
							credentials!!</label>



					</div>

					<div id="startpageLoadingGif" class="modal fade" role="dialog">
						<div class="modal-dialog"
							style="width: 150px; height: 150px; margin-top: 20px">
							<!-- Modal content-->
							<div class="modal-content">
								<div class="modal-body" style="">
									<div class="row" style="text-align: center;">
										<label> Please wait we are processing ! </label> <img
											src="<c:url value='/resources/images/startpageLoadingGif.gif'/>" />
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>

</body>
</html>