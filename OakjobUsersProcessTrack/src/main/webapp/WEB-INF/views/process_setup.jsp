<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%
	session = request.getSession(false);

	if (session != null && session.getAttribute("userName") != null
			&& session.getAttribute("userName").equals("test")) {
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script
	src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.23/jquery-ui.min.js"></script>
<script
	src="http://cdn.jsdelivr.net/g/jquery@1,jquery.ui@1.10%28jquery.ui.core.min.js+jquery.ui.widget.min.js+jquery.ui.mouse.min.js+jquery.ui.sortable.min.js%29,angularjs@1.2,angular.ui-sortable"></script>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script
	src="http://ajax.googleapis.com/ajax/libs/angularjs/1.2.26/angular.min.js"></script>
<script
	src="//cdnjs.cloudflare.com/ajax/libs/angular-ui-bootstrap/0.11.0/ui-bootstrap-tpls.min.js"></script>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<link rel="stylesheet"
	href="//netdna.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">
<%-- <link
	href="${pageContext.servletContext.contextPath}/index.css"
	rel="stylesheet">
	 --%>
<link rel="stylesheet" href="resources/css/process_setup.css"
	type="text/css" />
<script src="resources/js/process_setup.js"></script>
<title>Insert title here</title>
</head>
<body>

	<h1 class="header">PROCESS SETUP</h1>
	<!-- ng-app="myApp"	ng-controller="myAppController"
	style="text-align: center; background: #fbfafa;"
	 -->

	<div ng-app="myApp" ng-controller="myProcess_Controller"
		ng-init="initilizePathVariable('${pageContext.servletContext.contextPath}')"
		style="text-align: center; background: #fbfafa;">
		<p style="margin-top: 103px; display: inline-block; width: 100%;">
			<a
				href="http://oakjobalerts.com:8080/OakjobUsersProcessTrack/all_important_links"
				target="#">Click Here for all important links</a>
		</p>

		<div class="form_div">
			<img src="http://oakjobalerts.com/img/logo.png" alt="Mountain View"
				style="width: 150px; height: 150px;">

			<form class="form-horizontal">
				<div class="form-group">
					<label class="control-label col-sm-2" for="email"> Process:</label>
					<div class="col-sm-10">
						<select ng-model="processModel.whitelable_name"
							class="form-control"
							ng-options="item for item in processModel.whitelabelList"
							ng-change="updateDomainWiseArray(processModel.whitelable_name)"></select>
					</div>
				</div>


				</select>
				<div class="form-group">
					<label class="control-label col-sm-2">Provider:</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="entered_provider"
							placeholder="Enter Provider"
							ng-model="processModel.entered_provider">
					</div>
				</div>
				<h4>OR</h4>
				<div class="form-group">
					<label class="control-label col-sm-2" for="pwd"> Advt:</label>
					<div class="col-sm-10">
						<select ng-model="processModel.advertisement" class="form-control"
							ng-options="item for item in processModel.advertiseMents"
							ng-change=""></select>
					</div>
				</div>



				<div class="form-group">
					<label class="control-label col-sm-2" for="pwd"> Type</label>
					<div class="col-sm-10">
						<select ng-model="processModel.process_Type" class="form-control"
							ng-options="item for item in processType" ng-change=""></select>
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-sm-2" for="pwd"> Fresher:</label>
					<div class="col-sm-10">
						<select ng-model="processModel.fresher_domain"
							class="form-control"
							ng-options="item for item in whitelabels_domainList" ng-change=""></select>
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-sm-2" for="pwd"> Opener</label>
					<div class="col-sm-10">
						<select ng-model="processModel.opener_domain" class="form-control"
							ng-options="item for item in whitelabels_domainList" ng-change=""></select>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-sm-2" for="pwd"> Second:</label>
					<div class="col-sm-10">
						<select ng-model="processModel.fresher_second_doman"
							class="form-control"
							ng-options="item for item in whitelabels_domainList" ng-change=""></select>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-sm-2" for="pwd"> Weekly:</label>
					<div class="col-sm-10">
						<select ng-model="processModel.weekly_domain" class="form-control"
							ng-options="item for item in whitelabels_domainList" ng-change=""></select>
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-sm-2" for="pwd"> Queue:</label>
					<div class="col-sm-10">
						<select ng-model="processModel.queue_name" class="form-control"
							ng-options="item for item in processModel.queueNames"
							ng-change=""></select>
					</div>
				</div>





				<div class="form-group">
					<div class="col-sm-offset-2 col-sm-10">
						<button type="submit" class="btn btn-default"
							ng-click="addNewProcess(processModel)">Submit</button>
					</div>
				</div>
			</form>



		</div>

		<div id="startpageLoadingGif" class="modal fade" role="dialog">
			<div class="modal-dialog"
				style="width: 150px; height: 150px; margin-top: 20px">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-body" style="">
						<div class="row" style="text-align: center;">
							<label> Please wait we are processing ! </label> <img
								src="${pageContext.servletContext.contextPath}/resources/images/startpageLoadingGif.gif" />
						</div>
					</div>
				</div>
			</div>
		</div>

		<div id="responsealert" class="modal fade" role="dialog">
			<div class="modal-dialog modal-sm" style="margin-top: 100px">
				<!-- Modal content-->
				<div class="modal-content">

					<div class="modal-body" style="">
						<div class="row" style="text-align: center;">
							<label> {{responseData}} </label>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- <h1 class="footer"></h1> -->

</body>
</html>
<%
	} else {
		response.sendRedirect(request.getContextPath() + "/login");
	}
%>