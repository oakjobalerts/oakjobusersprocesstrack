<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	session = request.getSession(false);

	if (session != null && session.getAttribute("userName") != null
			&& session.getAttribute("userName").equals("test")) {
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- <script
	src="http://cdn.jsdelivr.net/g/jquery@1,jquery.ui@1.10%28jquery.ui.core.min.js+jquery.ui.widget.min.js+jquery.ui.mouse.min.js+jquery.ui.sortable.min.js%29,angularjs@1.2,angular.ui-sortable"></script>

 -->
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<!-- <script
	src="http://ajax.googleapis.com/ajax/libs/angularjs/1.2.26/angular.min.js"></script>

 -->

<script
	src="//cdnjs.cloudflare.com/ajax/libs/angular-ui-bootstrap/0.11.0/ui-bootstrap-tpls.min.js"></script>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<link rel="stylesheet"
	href="//netdna.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">


<script
	src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.5/angular.min.js"></script>
<link rel="stylesheet" href="resources/css/process_setup.css"
	type="text/css" />
<link rel="stylesheet"
	href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

<link
	href="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css"
	rel="stylesheet">
<script language="javascript"
	src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.7/angular.js"></script>
<script language="javascript"
	src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.7/angular-animate.js"></script>
<script language="javascript"
	src="http://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.14.3.js"></script>

<script type="text/javascript"
	src="https://code.highcharts.com/highcharts.js"></script>
<script src="resources/js/advertisement_wise_data.js"></script>
</head>
<body style="Padding: 20px;">



	<div ng-app="myApp" ng-controller="myCntrl"
		ng-init="initilizePathVariable('${pageContext.servletContext.contextPath}')"
		style="text-align: center; background: #fbfafa;">
		<p style="margin-top: 103px; display: inline-block; width: 100%;">
			<a
				href="http://oakjobalerts.com:8080/OakjobUsersProcessTrack/all_important_links"
				target="#">Click Here for all important links</a>
		</p>

		<div class="advertisement_form_div">
			<img src="http://oakjobalerts.com/img/logo.png" alt="Mountain View"
				style="width: 150px; height: 150px;">


			<form class="form-inline">
				From Date: <input type="text" uib-datepicker-popup="{{dateformat}}"
					ng-model="fromDate" is-open="showDp_FromDate" /> <span>
					<button type="button" class="btn btn-default"
						ng-click="showcalendar_fromDate($event)">
						<i class="glyphicon glyphicon-calendar"></i>
					</button>


				</span>To Date: <input type="text" uib-datepicker-popup="{{dateformat}}"
					ng-model="toDate" is-open="showDp_ToDate" /> <span>
					<button type="button" class="btn btn-default"
						ng-click="showcalendar_toDate($event)">
						<i class="glyphicon glyphicon-calendar"></i>
					</button>


				</span>
				<div class="form-group">
					<label for="email">Advt:</label> <select
						ng-model="advertisementModel.selectedAdvertisement"
						class="form-control"
						ng-options="item for item in advertisementModel.advertisements"
						ng-change="getNewWhitelable_List(advertisementModel)"></select>

				</div>
				<div class="form-group">
					<label for="email">Whitelabel:</label> <select
						ng-model="advertisementModel.selectedWhitelable"
						class="form-control"
						ng-options="item for item in advertisementModel.whitelables"
						ng-change=""></select>

				</div>


				<button type="Search" class="btn btn-default"
					ng-click="getAdvertisementWiseData(advertisementModel)">Search</button>
			</form>

		</div>
		<div class="container-span"
			style="height: 50px; text-align: center; margin-top: 15px;">
			<Span style="font-size: 30px; color: green;">Performance Graph</Span>
		</div>
		<div id="all_whitelable_graph_data"></div>
		<div id="startpageLoadingGif" class="modal fade" role="dialog">
			<div class="modal-dialog"
				style="width: 150px; height: 150px; margin-top: 20px">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-body" style="">
						<div class="row" style="text-align: center;">
							<label> Please wait we are processing ! </label> <img
								src="${pageContext.servletContext.contextPath}/resources/images/startpageLoadingGif.gif" />
						</div>
					</div>
				</div>
			</div>
		</div>

		<div id="responsealert" class="modal fade" role="dialog">
			<div class="modal-dialog modal-sm" style="margin-top: 100px">
				<!-- Modal content-->
				<div class="modal-content">

					<div class="modal-body" style="">
						<div class="row" style="text-align: center;">
							<label> {{responseData}} </label>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
<%
	} else {
		response.sendRedirect(request.getContextPath() + "/login");
	}
%>