<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script
	src="http://cdn.jsdelivr.net/g/jquery@1,jquery.ui@1.10%28jquery.ui.core.min.js+jquery.ui.widget.min.js+jquery.ui.mouse.min.js+jquery.ui.sortable.min.js%29,angularjs@1.2,angular.ui-sortable"></script>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script
	src="http://ajax.googleapis.com/ajax/libs/angularjs/1.2.26/angular.min.js"></script>
<script
	src="//cdnjs.cloudflare.com/ajax/libs/angular-ui-bootstrap/0.11.0/ui-bootstrap-tpls.min.js"></script>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<link rel="stylesheet"
	href="//netdna.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">
<%-- <link
	href="${pageContext.servletContext.contextPath}/index.css"
	rel="stylesheet">
	 --%>
<link rel="stylesheet" href="resources/css/process_setup.css"
	type="text/css" />
<link rel="stylesheet"
	href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="resources/js/advertisement_info.js"></script>
<title>Insert title here</title>
</head>
<body>


	<h1 class="header">ADVERTISEMENT WISE INFO</h1>
	<div ng-app="advertisement-info" ng-controller="advertisement_info"
		ng-init="initilizePathVariable('${pageContext.servletContext.contextPath}')"
		style="text-align: center; background: #fbfafa;">
		<div class="advertisement_form_div">
			<img src="http://oakjobalerts.com/img/logo.png" alt="Mountain View"
				style="width: 150px; height: 150px;">


			<form class="form-inline">
				<div class="form-group">
					<label for="email">From:</label> <input class="date-picker-style"
						ng-model="advertisementModel.fromDateSelected" type="text"
						id="fromDateSelected" ng-required="true">
				</div>
				<div class="form-group">
					<label for="email">To:</label> <input class="date-picker-style"
						ng-model="advertisementModel.toDateSelected" type="text"
						id="toDateSelected" ng-required="true">
				</div>
				<div class="form-group">
					<label for="email">Advt:</label> <select
						ng-model="advertisementModel.selectedAdvertisement"
						class="form-control"
						ng-options="item for item in advertisementModel.advertisements"
						ng-change=""></select>

				</div>
				<div class="form-group">
					<label for="email">Whitelabel:</label> <select
						ng-model="advertisementModel.selectedWhitelable"
						class="form-control"
						ng-options="item for item in advertisementModel.whitelables"
						ng-change=""></select>

				</div>


				<button type="Search" class="btn btn-default"
					ng-click="getAdvertisementWiseData(advertisementModel)">Search</button>
			</form>

		</div>
		<div class="container-span"
			style="height: 50px; text-align: center; margin-top: 15px;">
			<Span style="font-size: 30px; color: green;">Performance Graph</Span>
		</div>
		<div class="container" style="margin-top: 30px;">
			<div class="row">
				<div id="space-for-highcharts"></div>

			</div>
		</div>
		<div id="startpageLoadingGif" class="modal fade" role="dialog">
			<div class="modal-dialog"
				style="width: 150px; height: 150px; margin-top: 20px">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-body" style="">
						<div class="row" style="text-align: center;">
							<label> Please wait we are processing ! </label> <img
								src="${pageContext.servletContext.contextPath}/resources/images/startpageLoadingGif.gif" />
						</div>
					</div>
				</div>
			</div>
		</div>

		<div id="responsealert" class="modal fade" role="dialog">
			<div class="modal-dialog modal-sm" style="margin-top: 100px">
				<!-- Modal content-->
				<div class="modal-content">

					<div class="modal-body" style="">
						<div class="row" style="text-align: center;">
							<label> {{responseData}} </label>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

</body>
</html>