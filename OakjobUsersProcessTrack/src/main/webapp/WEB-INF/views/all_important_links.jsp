<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	session = request.getSession(false);

	if (session != null && session.getAttribute("userName") != null
			&& session.getAttribute("userName").equals("test")) {
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<link rel="stylesheet" href="resources/css/provider_mainprovider.css"
	type="text/css" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>

	<h1 class="header">All important Links</h1>

	<div class="modal-body row" style="text-align: center;">
		<div class="form_div" style="margin-top: 180px">
			<img src="http://oakjobalerts.com/img/logo.png" alt="Mountain View"
				style="width: 150px; height: 150px;">
			<div class="clearfix"></div>


			<p style="margin-top: 87px; display: inline-block; width: 100%;">
				<a
					href="http://oakjobalerts.com:8080/OakjobUsersProcessTrack/advertisement_wise_data"
					target="#">Click Here for Advt</a>
			</p>

			<p style="margin-top: -6px; display: inline-block; width: 100%;">
				<a
					href="http://oakjobalerts.com:8080/EditAlertInterface/rampup-page"
					target="#">Click Here for Rampup</a>
			</p>

			<p style="margin-top: -6px; display: inline-block; width: 100%;">
				<a
					href="http://oakjobalerts.com:8080/OakjobUsersProcessTrack/process_setup"
					target="#">Click Here To Setup Process</a>
			</p>
			<p style="margin-top: -6px; display: inline-block; width: 100%;">

				<a
					href="http://oakjobalerts.com:8080/OakjobUsersProcessTrack/chartGraph_providers"
					target="#">Click Here To Check Existing Providers</a>
			</p>
		</div>
</body>
</html>
<%
	} else {
		response.sendRedirect(request.getContextPath() + "/login");
	}
%>