<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%
	session = request.getSession(false);

	if (session != null && session.getAttribute("userName") != null
			&& session.getAttribute("userName").equals("test")) {
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- <script
	src="http://cdn.jsdelivr.net/g/jquery@1,jquery.ui@1.10%28jquery.ui.core.min.js+jquery.ui.widget.min.js+jquery.ui.mouse.min.js+jquery.ui.sortable.min.js%29,angularjs@1.2,angular.ui-sortable"></script>

 -->
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<!-- <script
	src="http://ajax.googleapis.com/ajax/libs/angularjs/1.2.26/angular.min.js"></script>

 -->

<script
	src="//cdnjs.cloudflare.com/ajax/libs/angular-ui-bootstrap/0.11.0/ui-bootstrap-tpls.min.js"></script>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<link rel="stylesheet"
	href="//netdna.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">


<script
	src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.5/angular.min.js"></script>
<link rel="stylesheet" href="resources/css/process_setup.css"
	type="text/css" />
<link rel="stylesheet"
	href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

<link
	href="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css"
	rel="stylesheet">
<script language="javascript"
	src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.7/angular.js"></script>
<script language="javascript"
	src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.7/angular-animate.js"></script>
<script language="javascript"
	src="http://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.14.3.js"></script>

<script src="resources/js/all_seekers_info.js"></script>
</head>
<body>
	<h1 class="header">SEEKERS INFO</h1>

	<div ng-app="seekers_info" ng-controller="seekers_info_controller"
		ng-init="initilizePathVariable('${pageContext.servletContext.contextPath}')"
		style="text-align: center; background: #fbfafa;">

		<div id="startpageLoadingGif" class="modal fade" role="dialog">
			<div class="modal-dialog"
				style="width: 150px; height: 150px; margin-top: 20px">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-body" style="">
						<div class="row" style="text-align: center;">
							<label> Please wait we are processing ! </label> <img
								src="${pageContext.servletContext.contextPath}/resources/images/startpageLoadingGif.gif" />
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>

		<div class="col-md-12">
			<div class="col-md-6">
				<p style="margin-top: 103px; display: inline-block; width: 100%;">
					<a
						href="http://oakjobalerts.com:8080/OakjobUsersProcessTrack/all_important_links"
						target="#">Click Here for all important links</a>
				</p>



			</div>
			<div class="col-md-6" style="margin-top: 120px; margin-bottom: 16px">

				<input type="text" class="form-control" id="seekers_name"
					placeholder="Search for seeker" style="width: 184px"
					ng-model="searchedSeeker"
					ng-change="updateSeekersList_accordingly(searchedSeeker)">


			</div>


		</div>

		<div class="clearfix"></div>
		<div class="table_div table-wrapper">
			<table class="tableClass table table-striped seekersInfo">
				<tr>
					<th>Name</th>
					<th>Channel</th>
					<th>Postal Address</th>
					<th>Image Postal Address</th>
					<th>SubjectFromName</th>
					<th>Arbor_pixel</th>
				</tr>
				<tr ng-repeat="seekers in SeekersModelist">
					<td>{{ seekers.seekerName }}</td>
					<td>{{ seekers.channel }}</td>
					<td>{{ seekers.postal_address }}</td>
					<td>{{ seekers.image_postal_address }}</td>
					<td>{{ seekers.subjectAndFromName }}</td>
					<td>{{ seekers.arbor_pixel }}</td>



				</tr>
			</table>
		</div>
		<div class="col-md-12" style="margin-top: 54px;">
			<button class="button btn btn-success btn-lg" id="addnew"
				ng-click="showDialogToAdd_newseeker()">Add New</button>
		</div>

		<!-- Modal -->
		<div id="myModal" class="modal fade" role="dialog">
			<div class="modal-dialog">

				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Add New Seeker</h4>
					</div>
					<div class="modal-body">
						<div class="col-md-12">
							<div class="col-md-6">
								<input type="text" class="form-control" id="new_seekers_name"
									placeholder="Enter new seeker" style="width: 184px"
									ng-model="newSeekerModel.seekerName"> <input
									type="text" class="form-control" id="new_seekers_name"
									placeholder="Enter Channel"
									style="width: 184px; margin-top: 20px;"
									ng-model="newSeekerModel.channel"> <input type="text"
									class="form-control" id="new_seekers_name"
									placeholder="Enter Postal Address"
									style="width: 184px; margin-top: 20px;"
									ng-model="newSeekerModel.postal_address"> <input
									type="text" class="form-control" id="new_seekers_name"
									placeholder="Enter image postal address"
									style="width: 184px; margin-top: 20px;"
									ng-model="newSeekerModel.image_postal_address"> <input
									type="text" class="form-control" id="new_seekers_name"
									placeholder="Enter Subject file name"
									style="width: 184px; margin-top: 20px;"
									ng-model="newSeekerModel.subjectAndFromName"> <input
									type="text" class="form-control" id="new_seekers_name"
									placeholder="Enter arbor pixel"
									style="width: 184px; margin-top: 20px;"
									ng-model="newSeekerModel.arbor_pixel">

							</div>
							<div class="col-md-6">
								<select ng-model="tempSeekerName" class="form-control"
									ng-options="item for item in oldSeekerModel.seekerList"
									ng-change="updateSeekerNameFromOld(tempSeekerName)"></select> <select
									ng-model="tempSelectedchannel" class="form-control"
									ng-options="item for item in oldSeekerModel.channelList"
									ng-change="" style="margin-top: 20px;"></select> <select
									ng-model="tempSelectedPostal" class="form-control"
									ng-options="item for item in oldSeekerModel.postal_addressList"
									ng-change="" style="margin-top: 20px;"></select> <select
									ng-model="tempSelectedimage_postal" class="form-control"
									ng-options="item for item in oldSeekerModel.image_postal_addressList"
									ng-change="" style="margin-top: 20px;"></select> <select
									ng-model="tempSelectedSubjectFromName" class="form-control"
									ng-options="item for item in oldSeekerModel.subjectAndFromNameList"
									ng-change="" style="margin-top: 20px;"></select> <select
									ng-model="tempSelectedArbor" class="form-control"
									ng-options="item for item in oldSeekerModel.arbor_pixelList"
									ng-change="" style="margin-top: 20px;"></select>

							</div>


						</div>
						<div class="col-md-12" style="margin-top: 54px;">
							<button class="button btn btn-success btn-lg" id="add"
								ng-click="addNewSeekerInTable(newSeekerModel)">Add</button>


						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default"
								data-dismiss="modal">Close</button>
						</div>
					</div>

				</div>
			</div>


			<div id="responsealert" class="modal fade" role="dialog">
				<div class="modal-dialog modal-sm" style="margin-top: 100px">
					<!-- Modal content-->
					<div class="modal-content">

						<div class="modal-body" style="">
							<div class="row" style="text-align: center;">
								<label> {{responseData}} </label>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
</body>
</html>
<%
	} else {
		response.sendRedirect(request.getContextPath() + "/login");
	}
%>