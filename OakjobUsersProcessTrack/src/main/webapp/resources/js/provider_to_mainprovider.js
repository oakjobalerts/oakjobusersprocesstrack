/**
 * in this we will maintain the list of provider and their main provider
 */

var myApp = angular.module('provider_to_main_module', [ "xeditable" ]);

myApp
		.controller(
				"provider_controller",
				function($scope, $http, $timeout, $filter, $window) {

					$scope.pathVariable = "";
					$scope.providerMainModel = {};
					$scope.allProviderList = [];
					$scope.newProviderModel = {};
					$scope.isUpdateRequired = false;
					$scope.initializer = function(pathVariable) {

						$scope.pathVariable = pathVariable;
						console.log("provider initialze success fully"
								+ pathVariable
								+ "/provider_to_mainprovider_info");

						$('#startpageLoadingGif').modal({
							backdrop : 'static',
							keyboard : false
						});
						var response = $http({
							method : 'GET',
							url : $scope.pathVariable
									+ "/provider_to_mainprovider_info/"

						});

						response
								.success(function(data, status, headers, config) {

									console.log("result=" + data);

									$scope.allProviderList = data;

									$('#startpageLoadingGif').modal('hide');

								});

						response.error(function(data, status, headers, config) {
							$('#startpageLoadingGif').modal('hide');
							alert(data);
						});

					}

					$scope.update_MainProvider_with_Provider = function(
							providerMainModel) {

						console.log("updating main provider");

						$scope.providerMainModel.main_provider = providerMainModel.provider_to_main_provider_map[providerMainModel.provider];

					}

					$scope.refreshPorviderList = function() {

						console.log("refreshing  provider");

						$scope.providerMainModel.providerList = $scope.allProviderList;

					}

					$scope.change_updating_parameter = function(providerModel) {
						console
								.log("value get change for "
										+ providerModel.providerName
										+ " and its position="
										+ providerModel.position);
						providerModel.update = true;
						$scope.allProviderList[providerModel.position] = providerModel;

					}

					$scope.updateProviderList = function(allProviderList) {

						for (var i = 0; i < allProviderList.length; i++) {
							if (allProviderList[i].update) {
								$scope.isUpdateRequired = true;
								console.log("update required for"
										+ allProviderList[i].providerName);
								break;
							}

						}

						if ($scope.isUpdateRequired == true) {

							$('#startpageLoadingGif').modal({
								backdrop : 'static',
								keyboard : false
							});
							var response = $http({
								method : 'POST',
								url : $scope.pathVariable
										+ "/update_provider_list/",
								data : $scope.allProviderList,

							});

							response
									.success(function(data, status, headers,
											config) {

										if (data == null) {
											$scope.responseData = "We face some internal error . Please refresh the page!!";

										} else {

											$scope.allProviderList = data;
											$scope.isUpdateRequired = false;
											console.log("result=" + data);
											$scope.responseData = "Record has been updated successfully";

										}

										//

										$('#startpageLoadingGif').modal('hide');

										$("#responsealert")
												.fadeTo(2000, 500)
												.slideUp(
														500,
														function() {
															$("#responsealert")
																	.alert(
																			'close');
														});

									});

							response.error(function(data, status, headers,
									config) {
								$('#startpageLoadingGif').modal('hide');
								alert(data);
							});

						} else {
							$scope.responseData = "No Change found to update!!";
							$("#responsealert").fadeTo(2000, 500).slideUp(500,
									function() {
										$("#responsealert").alert('close');
									});
						}

					}

					$scope.show_add_dialog = function() {

						$('#myModal').modal({
							backdrop : 'static',
							keyboard : false
						});
					}
					$scope.addNew_provider_Record = function(providerMainModel) {

						console.log("in adding the data");

						if (providerMainModel.providerName == null
								|| providerMainModel.providerName == ""
								|| providerMainModel.providerName == "null") {
							console.log("allready exist");
							$scope.responseData = "Provider can not be empty or null";
							document.getElementById("new_provider_name").value = "";

							$("#responsealert").fadeTo(2000, 500).slideUp(500,
									function() {
										$("#responsealert").alert('close');
									});

						}

						else if (providerMainModel.displayProviderName == null
								|| providerMainModel.displayProviderName == ""
								|| providerMainModel.displayProviderName == "null") {
							console.log("allready exist");
							$scope.responseData = "MainProvider can not be empty or null";
							document
									.getElementById("new_display_provider_name").value = "";

							$("#responsealert").fadeTo(2000, 500).slideUp(500,
									function() {
										$("#responsealert").alert('close');
									});

						} else {

							console.log("going to add data");
							$('#startpageLoadingGif').modal({
								backdrop : 'static',
								keyboard : false
							});
							var response = $http({
								method : 'POST',
								url : $scope.pathVariable
										+ "/add_new_provider_to_mainProvider/",
								data : providerMainModel

							});

							response.success(function(data, status, headers,
									config) {

								console.log("result=" + data);
								$scope.allProviderList.push(data);
								$scope.responseData = ""
										+ providerMainModel.providerName
										+ " and "
										+ providerMainModel.displayProviderName
										+ " has been added successfully";

								$('#startpageLoadingGif').modal('hide');
								$('#myModal').modal('hide');

								$("#responsealert").fadeTo(2000, 500).slideUp(
										500, function() {
											$("#responsealert").alert('close');
										});

							});

							response.error(function(data, status, headers,
									config) {
								$('#startpageLoadingGif').modal('hide');
								alert(data);
							});
						}

					}

				});
