var myApp = angular.module('advertisement-info', [ 'ngAnimate', 'ui.bootstrap' ]);

myApp
		.controller(
				"advertisement_info",
				function($scope, $http, $timeout, $filter, $window) {

					$scope.advertisementModel = {};
					$scope.searchResultModel = {};

					$scope.pathVariable = "";
					$scope.selectedWhitelable = "";
					
					$scope.today = function() {
						$scope.dt = new Date();
					};
					$scope.dateformat = "yyyy-MM-dd";
					$scope.today();
					$scope.showcalendar = function($event) {
						$scope.showdp = true;
					};
					$scope.showdp = false;

					$scope.printDate = function() {
						console.log($scope.dt);
					}

					$scope.initilizePathVariable = function(pathVariable) {
						$scope.pathVariable = pathVariable;
						console.log("advertisement initialized successfully!!"
								+ pathVariable + "/advertisement_info");

						$('#startpageLoadingGif').modal({
							backdrop : 'static',
							keyboard : false
						});
						var response = $http({
							method : 'GET',
							url : $scope.pathVariable + "/advertisement_info/"

						});

						response
								.success(function(data, status, headers, config) {
									console.log("date=" + data)
									$scope.advertisementModel = data;

									$scope.advertisementModel.fromDateSelected = $filter(
											'date')(new Date(), 'yyyy-MM-dd');
									$scope.advertisementModel.toDateSelected = $filter(
											'date')(new Date(), 'yyyy-MM-dd');

									console
											.log($scope.advertisementModel.fromDateSelected);
									console
											.log($scope.advertisementModel.selectedAdvertisement);
									$('#startpageLoadingGif').modal('hide');

								});

						response.error(function(data, status, headers, config) {
							$('#startpageLoadingGif').modal('hide');
							alert(data);
						});

					}

					$scope.getAdvertisementWiseData = function(
							advertisementModel) {
						console.log("searching for data");

						console.log(advertisementModel.toDateSelected + " and "
								+ advertisementModel.fromDateSelected);

						$('#startpageLoadingGif').modal({
							backdrop : 'static',
							keyboard : false
						});

						var response = $http({
							method : 'POST',
							url : $scope.pathVariable + "/search_result/",
							data : advertisementModel

						}

						);
						response
								.success(function(data, status, headers, config) {
									$('#startpageLoadingGif').modal('hide');
									console.log("search result=" + data);
									$scope.searchResultModel = data;

									$scope
											.hashMapIterator_Function($scope.searchResultModel.rampup_register_users);
									$scope
											.hashMapIterator_Function($scope.searchResultModel.rampup_freshers_users);

									$scope
											.hashMapIterator_Function($scope.searchResultModel.rampup_opener_users);

									$scope
											.hashMapIterator_Function($scope.searchResultModel.rampup_clicker_users);

								});

						response.error(function(data, status, headers, config) {
							$('#startpageLoadingGif').modal('hide');
							alert(data);
						});
					}

					$scope.hashMapIterator_Function = function(hashMap) {
						$scope.firstValueFlag = true;

						// iteratinog hash map in angular
						angular.forEach(hashMap, function(value, key) {

							console.log("key=" + key + " and value=" + value);

						})
					}

				});

$(function() {

	$("#datepicker_startdate").datepicker({
		dateFormat : 'yy-mm-dd'
	});
	$("#datepicker_startdate").datepicker("setDate", 'today');

	$("#datepicker_startdate").datepicker({
		dateFormat : 'yy-mm-dd'
	});
});
$(function() {

	$("#datepicker_enddate").datepicker({
		dateFormat : 'yy-mm-dd'
	});
	$("#datepicker_enddate").datepicker("setDate", 'today');

	$("#datepicker_enddate").datepicker({
		dateFormat : 'yy-mm-dd'
	});
});
