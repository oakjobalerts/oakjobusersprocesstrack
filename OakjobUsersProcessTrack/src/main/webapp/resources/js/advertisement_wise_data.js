angular.module('myApp', [ 'ngAnimate', 'ui.bootstrap' ]);
angular
		.module('myApp')
		.controller(
				'myCntrl',
				function($scope, $http, $timeout, $filter, $window) {

					$scope.advertisementModel = {};
					$scope.searchResultModel = {};
					$scope.searchResultModel_Array = [];

					$scope.category = [];
					$scope.registerSerisData = [];
					$scope.fresherSerisData = [];
					$scope.openerSeriesData = [];
					$scope.clickerSeriesData = [];

					$scope.pathVariable = "";
					$scope.selectedWhitelable = "";

					$scope.dateformat = "yyyy-MM-dd";
					$scope.showDp_FromDate = false;
					$scope.showDp_ToDate = false;

					$scope.fromDate = new Date();
					$scope.toDate = new Date();

					$scope.showcalendar_fromDate = function($event) {
						$scope.showDp_FromDate = true;
						$scope.showDp_ToDate = false;
					};
					$scope.showcalendar_toDate = function($event) {
						$scope.showDp_FromDate = false;
						$scope.showDp_ToDate = true;
					};

					$scope.printDate = function() {
						console.log($scope.dt);
					}

					$scope.initilizePathVariable = function(pathVariable) {
						$scope.pathVariable = pathVariable;
						console.log("advertisement initialized successfully!!"
								+ pathVariable + "/advertisement_info");

						$('#startpageLoadingGif').modal({
							backdrop : 'static',
							keyboard : false
						});
						var response = $http({
							method : 'GET',
							url : $scope.pathVariable + "/advertisement_info/"

						});

						response
								.success(function(data, status, headers, config) {
									console.log("date=" + data)
									$scope.advertisementModel = data;

									$scope.advertisementModel.fromDateSelected = $filter(
											'date')(new Date(), 'yyyy-MM-dd');
									$scope.advertisementModel.toDateSelected = $filter(
											'date')(new Date(), 'yyyy-MM-dd');

									console
											.log($scope.advertisementModel.fromDateSelected);
									console
											.log($scope.advertisementModel.selectedAdvertisement);

									$scope
											.getNewWhitelable_List($scope.advertisementModel);
									$('#startpageLoadingGif').modal('hide');

								});

						response.error(function(data, status, headers, config) {
							$('#startpageLoadingGif').modal('hide');
							alert(data);
						});

					}

					$scope.getAdvertisementWiseData = function(
							advertisementModel) {
						console.log("searching for data");

						advertisementModel.fromDateSelected = $filter('date')(
								$scope.fromDate, 'yyyy-MM-dd');
						advertisementModel.toDateSelected = $filter('date')(
								$scope.toDate, 'yyyy-MM-dd');

						console.log(advertisementModel.toDateSelected + " --"
								+ advertisementModel.fromDateSelected);

						$('#startpageLoadingGif').modal({
							backdrop : 'static',
							keyboard : false
						});

						var response = $http({
							method : 'POST',
							url : $scope.pathVariable + "/search_result/",
							data : advertisementModel

						}

						);
						response
								.success(function(data, status, headers, config) {
									$('#startpageLoadingGif').modal('hide');
									console.log("search result=" + data);
									$scope.searchResultModel_Array = data;

									var myNode = document.getElementById("all_whitelable_graph_data");
									while (myNode.firstChild) {
									    myNode.removeChild(myNode.firstChild);
									}
									
									for (var i = 0; i < $scope.searchResultModel_Array.length; i++) {
										
										// Now create and append to iDiv
										var innerDiv = document.createElement('div');
										innerDiv.id = $scope.searchResultModel_Array[i].whitelabelName;
										// The variable iDiv is still good... Just append to it.
										document
										.getElementById("all_whitelable_graph_data").appendChild(innerDiv)
										
										
										$scope
												.hashMapIterator_Function($scope.searchResultModel_Array[i]);

										Highcharts
												.chart(
														$scope.searchResultModel_Array[i].whitelabelName,
														{

															xAxis : {
																categories : $scope.searchResultModel_Array[i].dataInterValList
															},
															title:
																{
																text:$scope.searchResultModel_Array[i].whitelabelName
																},

															series : [

																	{
																		data : $scope.searchResultModel_Array[i].registerSerisData,
																		name : "Register"
																	},
																	{
																		data : $scope.searchResultModel_Array[i].fresherSerisData,
																		name : "Fresher"
																	}

																	,
																	{
																		data : $scope.searchResultModel_Array[i].openerSeriesData,
																		name : "Opener"

																	}

																	,
																	{
																		data : $scope.searchResultModel_Array[i].clickerSeriesData,
																		name : "Clicker"

																	} ]
														});
									}

								});

						response.error(function(data, status, headers, config) {
							$('#startpageLoadingGif').modal('hide');
							alert(data);
						});
					}

					$scope.hashMapIterator_Function = function(
							searchResultModel) {

						for (var i = 0; i < searchResultModel.dataInterValList.length; i++) {

							if (searchResultModel.rampup_register_users[searchResultModel.dataInterValList[i]] == null) {

								searchResultModel.registerSerisData.push("0");

							} else {
								searchResultModel.registerSerisData
										.push(searchResultModel.rampup_register_users[searchResultModel.dataInterValList[i]])

							}

							if (searchResultModel.rampup_freshers_users[searchResultModel.dataInterValList[i]] == null) {
								searchResultModel.fresherSerisData.push("0");
							} else {
								searchResultModel.fresherSerisData
										.push(searchResultModel.rampup_freshers_users[searchResultModel.dataInterValList[i]])

							}

							if (searchResultModel.rampup_opener_users[searchResultModel.dataInterValList[i]] == null) {
								searchResultModel.openerSeriesData.push("0");
							} else {
								searchResultModel.openerSeriesData
										.push(searchResultModel.rampup_opener_users[searchResultModel.dataInterValList[i]])

							}
							if (searchResultModel.rampup_clicker_users[searchResultModel.dataInterValList[i]] == null) {
								searchResultModel.clickerSeriesData.push("0");
							} else {
								searchResultModel.clickerSeriesData
										.push(searchResultModel.rampup_clicker_users[searchResultModel.dataInterValList[i]])

							}
						}

						console.log(searchResultModel.registerSerisData + "--"
								+ searchResultModel.fresherSerisData + "=="
								+ searchResultModel.openerSeriesData + "==="
								+ searchResultModel.clickerSeriesData)

						return searchResultModel;

					}

					$scope.getNewWhitelable_List = function(advertisementModel) {
						advertisementModel.whitelables = [];
						advertisementModel.whitelables.push("All");
						$scope.tempArray = [];

						$scope.tempArray = advertisementModel.advertisement_wise_whitelables_list[advertisementModel.selectedAdvertisement];
						for (var int = 0; int < $scope.tempArray.length; int++) {
							advertisementModel.whitelables
									.push($scope.tempArray[int]);
						}

						advertisementModel.selectedWhitelable = advertisementModel.whitelables[0];

						console.log(advertisementModel);
					}

				});