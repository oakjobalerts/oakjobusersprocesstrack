/**
 * 
 */

var myApp = angular.module("seekers_info", []);

myApp
		.controller(
				"seekers_info_controller",
				function($scope, $http, $timeout, $filter, $window) {

					$scope.pathVariable = "";
					$scope.searchedSeeker = "";
					$scope.SeekersModelist = [];
					$scope.CompleteSeekersModelist = [];
					$scope.oldSeekerModel = {};
					$scope.fullSeekersModel = {};
					$scope.newSeekerModel = {};

					$scope.tempSeekerName = "";
					$scope.tempSelectedchannel = "";
					$scope.tempSelectedPostal = "";
					$scope.tempSelectedimage_postal = "";
					$scope.tempSelectedSubjectFromName = "";
					$scope.tempSelectedArbor = "";

					$scope.initilizePathVariable = function(pathVariable) {

						$scope.pathVariable = pathVariable;

						console.log("advertisement initialized successfully!!"
								+ pathVariable + "/seekers_info");

						$('#startpageLoadingGif').modal({
							backdrop : 'static',
							keyboard : false
						});
						var response = $http({
							method : 'GET',
							url : $scope.pathVariable + "/seekers_info/"

						});

						response
								.success(function(data, status, headers, config) {
									console.log("date=" + data)
									$scope.fullSeekersModel = data;
									$scope.SeekersModelist = $scope.fullSeekersModel.seekersList;
									$scope.CompleteSeekersModelist = $scope.fullSeekersModel.seekersList;

									$scope.oldSeekerModel = $scope.CompleteSeekersModelist[0];
									$scope.tempSeekerName = $scope.oldSeekerModel.seekerList[0];

									$scope
											.initializeTempObjectData($scope.oldSeekerModel);

									for (var i = 0; i < $scope.SeekersModelist.length; i++) {

										console
												.log("value of "
														+ i
														+ " ="
														+ $scope.SeekersModelist[i].seekerName);
									}

									$('#startpageLoadingGif').modal('hide');

								});

						response.error(function(data, status, headers, config) {
							$('#startpageLoadingGif').modal('hide');
							alert(data);
						});

					}

					$scope.updateSeekersList_accordingly = function(
							searchedSeeker) {

						console.log("seekerName" + searchedSeeker);

						if (searchedSeeker.length > 0) {
							console.log("search list");
							$scope.SeekersModelist = [];

							for (var i = 0; i < $scope.CompleteSeekersModelist.length; i++) {

								if ($scope.CompleteSeekersModelist[i].seekerName
										.match(searchedSeeker)) {
									console.log("added");

									$scope.SeekersModelist
											.push($scope.CompleteSeekersModelist[i]);

								}
							}

						} else {
							console.log("full list");
							$scope.SeekersModelist = $scope.CompleteSeekersModelist;
						}

					}

					$scope.showDialogToAdd_newseeker = function() {

						$('#myModal').modal({
							backdrop : 'static',
							keyboard : false
						});

						$scope.newSeekerModel.seekerName = "";
						$scope.newSeekerModel.channel = "";
						$scope.newSeekerModel.postal_address = "";
						$scope.newSeekerModel.image_postal_address = "";
						$scope.newSeekerModel.subjectAndFromName = "";
						$scope.newSeekerModel.arbor_pixel = "";

					}

					$scope.initializeTempObjectData = function(oldModel) {

						$scope.tempSeekerName = oldModel.seekerName;
						$scope.tempSelectedchannel = oldModel.channel;
						$scope.tempSelectedPostal = oldModel.postal_address;
						$scope.tempSelectedimage_postal = oldModel.image_postal_address;
						$scope.tempSelectedSubjectFromName = oldModel.subjectAndFromName;
						$scope.tempSelectedArbor = oldModel.arbor_pixel;

					}

					$scope.updateSeekerNameFromOld = function(seekerName) {

						console
								.log("seekers to be updated="
										+ seekerName
										+ "value="
										+ $scope.fullSeekersModel.seekers_map[seekerName].seekerName);
						$scope.newSeekerModel = $scope.fullSeekersModel.seekers_map[seekerName];
						$scope.initializeTempObjectData($scope.newSeekerModel);

					}
					$scope.addNewSeekerInTable = function(newSeekerModel) {

						if ($scope.newSeekerModel.seekerName == ''
								|| $scope.newSeekerModel.channel == ''
								|| newSeekerModel.postal_address == ''
								|| newSeekerModel.image_postal_address == ''
								|| newSeekerModel.subjectAndFromName == ''
								|| newSeekerModel.arbor_pixel == '') {

							$scope.responseData = "Please provide complete information!!";

							$("#responsealert").fadeTo(2000, 500).slideUp(500,
									function() {
										$("#responsealert").alert('close');
									});

						}

						else {
							console.log("adding new data in table");
							$('#startpageLoadingGif').modal({
								backdrop : 'static',
								keyboard : false
							});
							var response = $http({
								method : 'POST',
								url : $scope.pathVariable
										+ "/add_new_seekers_info/",
								data : newSeekerModel

							});

							response.success(function(data, status, headers,
									config) {

								$('#startpageLoadingGif').modal('hide');

							});

							response.error(function(data, status, headers,
									config) {
								$('#startpageLoadingGif').modal('hide');
								alert(data);
							});

						}

					}

				});
