var myLoginApp = angular.module("loginApp", []);

myLoginApp.controller("loginController", function($scope, $http, $timeout,
		$filter) {

	$scope.userModel = {};
	$scope.userModel.userName = "";
	$scope.userModel.userPassword = "";
	$scope.isUserInValid = false;

	$scope.initilizePathVariable = function(pathVariable) {
		$scope.pathVariable = pathVariable;

	}

	$scope.loginuser = function(userModel) {

		console.log(userModel.userName);
		console.log(userModel.userPassword);

		if (userModel.userName.length == 0) {

			var userName = document.getElementById('userName');
			userName.style.backgroundColor = "#ff6666";
			return false;

		} else if (userModel.userPassword.length == 0) {
			var password = document.getElementById('userPassword');
			password.style.backgroundColor = "#ff6666";
			return false;

		} else {
			var userName = document.getElementById('userName');
			var password = document.getElementById('userPassword');
			userName.style.backgroundColor = "#ffffff";
			password.style.backgroundColor = "#ffffff";
			console.log("both entry found")

			$('#startpageLoadingGif').modal({
				backdrop : 'static',
				keyboard : false
			});

			var response = $http({
				method : 'POST',
				url : $scope.pathVariable + "/user_login/",
				data : $scope.userModel
			});

			response.success(function(data, status, headers, config) {

				// if user is invalid
				console.log(data)
				if (data == 'true') {
					$scope.isUserInValid = false;
					console.log("data is true")
					$scope.userModel.userName = "";
					$scope.userModel.userPassword = "";
					// here we are redirecting to home page
					window.location = $scope.pathVariable
							+ "/chartGraph_providers"
					$('#startpageLoadingGif').modal('hide');

				} 
				else if(data == 'internal'){
					// here we are redirecting to home page
					window.location = $scope.pathVariable
							+ "/all_important_links"
					$('#startpageLoadingGif').modal('hide');
					
				}
				else {
					console.log("data is false")
					$scope.userModel.userName = "";
					$scope.userModel.userPassword = "";
					$scope.isUserInValid = true;
					$('#startpageLoadingGif').modal('hide');
				}

			});
			response.error(function(data, status, headers, config) {
				$('#startpageLoadingGif').modal('hide');
				alert(data);
			});

		}

	}

});
