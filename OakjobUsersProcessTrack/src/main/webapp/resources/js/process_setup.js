/**
 * 
 */

var myApp = angular.module('myApp', []);
myApp
		.controller(
				"myProcess_Controller",
				function($scope, $http, $timeout, $filter, $window) {

					$scope.processModel = {};
					$scope.pathVariable = "";
					$scope.responseData = "";

					$scope.processType = [];
					$scope.whitelabels_domainList = [];

					$scope.processType.push("Daily");
					$scope.processType.push("Opener");
					$scope.processType.push("Clicker");
					$scope.processType.push("Weekly");
					$scope.processType.push("Monthly");
					$scope.processType.push("Web");

					$scope.selected_whitelable = "";
					$scope.selected_process_type = $scope.processType[0];
					$scope.selected_advertisement = "";
					$scope.selected_queuename = "";

					$scope.selected_fresher_domain = "";
					$scope.selected_opener_domain = "";
					$scope.selected_fresher_second_domain = "";
					$scope.selected_weekly_domain = "";

					$scope.initilizePathVariable = function(pathVariable) {
						$scope.pathVariable = pathVariable;
						console.log("controller initialized successfully!!"
								+ pathVariable + "/processData");

						$('#startpageLoadingGif').modal({
							backdrop : 'static',
							keyboard : false
						});

						var response = $http({
							method : 'GET',
							url : $scope.pathVariable + "/processData/"

						});

						response
								.success(function(data, status, headers, config) {
									console.log("date=" + data)
									$scope.processModel = data;

									// $scope.selected_advertisement =
									// $scope.processModel.advertiseMents[0];
									// $scope.selected_queuename =
									// $scope.processModel.queueNames[0];
									// $scope.selected_whitelable =
									// $scope.processModel.whitelabelList[0];
									$scope.initializeModelValue();

									$('#startpageLoadingGif').modal('hide');

								});

						response.error(function(data, status, headers, config) {
							$('#startpageLoadingGif').modal('hide');
							alert(data);
						});

					}

					$scope.addNewProcess = function(processModel) {
						$('#startpageLoadingGif').modal({
							backdrop : 'static',
							keyboard : false
						});

						console.log("WHitelable=" + $scope.selected_whitelable
								+ " advertisement="
								+ $scope.selected_advertisement
								+ " processType="
								+ $scope.selected_process_type + " queuename="
								+ $scope.selected_queuename + "provider="
								+ processModel.entered_provider);

						var response = $http({
							method : 'POST',
							url : $scope.pathVariable + "/addNewProcess/",
							data : $scope.processModel
						});
						headers = "Accept=*/*";

						response
								.success(function(data, status, headers, config) {
									console.log("data=" + data)
									$('#startpageLoadingGif').modal('hide');

									$scope.responseData = data;

									$("#responsealert")
											.fadeTo(2000, 500)
											.slideUp(
													500,
													function() {
														$("#responsealert")
																.alert('close');
													});

								});
					}

					$scope.hashMapIterator_Function = function() {
						$scope.firstValueFlag = true;

						// iteratinog hash map in angular
						angular.forEach($scope.myMainModel.whitelabel_map,
								function(value, key) {

									$scope.whitelabels_domainList.push(value);

								})
						$scope
								.fetchProvider_FromWhitelables($scope.userWhiteLabel);
					}

					$scope.updateDomainWiseArray = function(selected_whitelable) {

						$scope.processModel.fresher_second_doman = "";
						$scope.processModel.fresher_domain = "";
						$scope.processModel.opener_domain = "";
						$scope.processModel.weekly_domain = "";

						$scope.processModel.whitelable_name = selected_whitelable;
						$scope.processModel.advertisement = $scope.processModel.advertiseMents[0];
						$scope.processModel.process_Type = $scope.processType[0];
						$scope.processModel.queue_name = $scope.processModel.queueNames[0];
						$scope.processModel.table_name = $scope.processModel.whitelable_table_map[$scope.processModel.whitelable_name];
						$scope.whitelabels_domainList = $scope.processModel.whitelable_domainHashMap[selected_whitelable];

						for (var i = 0; i < $scope.whitelabels_domainList.length; i++) {

							console
									.log("Dmain ="
											+ $scope.whitelabels_domainList[i]
											+ " and its type"
											+ $scope.processModel.whitelable_domain_with_type[$scope.whitelabels_domainList[i]]);

							$scope.whitelabels_domainList[i] = $scope.whitelabels_domainList[i]
									+ "-"
									+ $scope.processModel.whitelable_domain_with_type[$scope.whitelabels_domainList[i]];

							if ($scope.whitelabels_domainList[i]
									.indexOf("second") != -1) {
								$scope.processModel.fresher_second_doman = $scope.whitelabels_domainList[i];

							} else if ($scope.whitelabels_domainList[i]
									.indexOf("daily") != -1) {
								$scope.processModel.fresher_domain = $scope.whitelabels_domainList[i];

							} else if ($scope.whitelabels_domainList[i]
									.indexOf("opener") != -1
									|| $scope.whitelabels_domainList[i]
											.indexOf("clicker") != -1) {
								$scope.processModel.opener_domain = $scope.whitelabels_domainList[i];

							} else if ($scope.whitelabels_domainList[i]
									.indexOf("weekly") != -1
									||

									$scope.whitelabels_domainList[i]
											.indexOf("monthly") != -1) {
								$scope.processModel.weekly_domain = $scope.whitelabels_domainList[i];

							}
						}

						if ($scope.processModel.fresher_domain == '') {
							$scope.processModel.fresher_domain = $scope.whitelabels_domainList[0];

						}
						if ($scope.processModel.fresher_second_doman == '') {
							$scope.processModel.fresher_second_doman = $scope.whitelabels_domainList[0];

						}
						if ($scope.processModel.opener_domain == '') {
							$scope.processModel.opener_domain = $scope.whitelabels_domainList[0];

						}
						if ($scope.processModel.weekly_domain == '') {
							$scope.processModel.weekly_domain = $scope.whitelabels_domainList[0];

						}

					}

					$scope.initializeModelValue = function() {

						$scope.processModel.fresher_second_doman = "";
						$scope.processModel.fresher_domain = "";
						$scope.processModel.opener_domain = "";
						$scope.processModel.weekly_domain = "";
						$scope.processModel.entered_provider = "";

						$scope.processModel.whitelable_name = $scope.processModel.whitelabelList[0];
						$scope.processModel.advertisement = $scope.processModel.advertiseMents[0];
						$scope.processModel.process_Type = $scope.processType[0];
						$scope.processModel.queue_name = $scope.processModel.queueNames[0];
						$scope.processModel.table_name = $scope.processModel.whitelable_table_map[$scope.processModel.whitelable_name];
						$scope.whitelabels_domainList = $scope.processModel.whitelable_domainHashMap[$scope.processModel.whitelable_name];

						for (var i = 0; i < $scope.whitelabels_domainList.length; i++) {

							console
									.log("Dmain ="
											+ $scope.whitelabels_domainList[i]
											+ " and its type"
											+ $scope.processModel.whitelable_domain_with_type[$scope.whitelabels_domainList[i]]);

							$scope.whitelabels_domainList[i] = $scope.whitelabels_domainList[i]
									+ "-"
									+ $scope.processModel.whitelable_domain_with_type[$scope.whitelabels_domainList[i]];

							if ($scope.whitelabels_domainList[i]
									.indexOf("second")) {
								$scope.processModel.fresher_second_doman = $scope.whitelabels_domainList[i];

							} else if ($scope.whitelabels_domainList[i]
									.indexOf("daily")) {
								$scope.processModel.fresher_domain = $scope.whitelabels_domainList[i];

							} else if ($scope.whitelabels_domainList[i]
									.indexOf("opener")
									|| $scope.whitelabels_domainList[i]
											.indexOf("clicker")) {
								$scope.processModel.opener_domain = $scope.whitelabels_domainList[i];

							} else if ($scope.whitelabels_domainList[i]
									.indexOf("weekly")) {
								$scope.processModel.weekly_domain = $scope.whitelabels_domainList[i];

							}
						}
						if ($scope.processModel.fresher_domain == '') {
							$scope.processModel.fresher_domain = $scope.whitelabels_domainList[0];

						}
						if ($scope.processModel.fresher_second_doman == '') {
							$scope.processModel.fresher_second_doman = $scope.whitelabels_domainList[0];

						}
						if ($scope.processModel.opener_domain == '') {
							$scope.processModel.opener_domain = $scope.whitelabels_domainList[0];

						}
						if ($scope.processModel.weekly_domain == '') {
							$scope.processModel.weekly_domain = $scope.whitelabels_domainList[0];

						}
					}

				});
